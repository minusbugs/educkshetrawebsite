    <!-- Course Detail -->
        <div class="col-md-12">
          <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
          <!-- Modal -->
          <div class="modal fade" id="course-detail-modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content col-md-12" style="padding: 0;">
                    <div class="flat-course flat-hover-zoom" style="width: 100%;margin: 0;padding: 0;">
                        <div class="featured-post text-center" style="background: #f9f9f9;">             
                            <div class="overlay">
                                <div class="link"></div>
                            </div>
                            <a><img src="images/index/course1.jpg" alt="Course1" style="width: 400px;margin: 0 auto;"></a>
                        </div><!-- /.featured-post -->
                        <div class="course-content">
                            <h4><a></a> </h4>
                            <div class="price"> Software</div>
                            <ul class="course-meta review">
                                <li class="review-stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </li>
                                <li>Rating</li>
                            </ul> 
                            <h4 class="title-2">About the course</h4>
                            <p class="c-content"></p>

                            <div class="row text-center">
                                <a class="flat-button bg-orange" href="edukshetra-contact.php" style="display: inline-block;background: #F44336;border-color: #F44336;">ENROLL THIS COURSE</a>
                            </div>
                        </div><!-- /.course-content -->
                    </div>
                </div>
              
            </div>
          </div>
        </div>
        <!-- End -->

    <!-- Footer -->
        <footer class="footer">  
            <div class="footer-widgets">   
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">  
                            <div class="widget widget-text">
                                <img src="images/blog/Footer-01.png" alt="image">
                                <ul>
                                    <li class="address">Tripunithura, Ernakulam Kerala</li>
                                    <li class="phone"><a href="#">+91-484-4022747</a></li>
                                    <li class="email"><a href="#">info@educkshetra.com</a></li>  
                                </ul> 
                            </div><!-- /.widget -->      
                        </div>

                        <div class="col-md-2">
                            <div class="widget widget_tweets clearfix">
                                <h5 class="widget-title">User Links</h5>
                                <ul class="link-left">
                                    <li>
                                        <a href="edukshetra-about.php">Why Educkshetra</a>
                                    </li>
                                    <li>
                                        <a href="edukshetra-contact.php">Contact</a>
                                    </li>
                                    <li>
                                        <a href="edukshetra-courses.php">Courses</a>
                                    </li>
                                    <li>
                                        <a href="edukshetra-about.php">FAQs</a>
                                    </li>
                                      <li>
                                        <a href="edukshetra-about.php">Placements</a>
                                    </li>
                                </ul>
                            </div><!-- /.widget-recent-tweets -->
                        </div><!-- /.col-md-2 -->

                        <div class="col-md-3">
                            <div class="widget widget_recent-courses">
                                <h5 class="widget-title">Recent Courses</h5>
                                <ul class="recent-courses-news clearfix">
                                    <li>
                                        <div class="thumb">
                                            <img src="images/blog/Footer-02.png" alt="image">
                                        </div>
                                        <div class="text">
                                            <a href="#">Embedded Courses</a>
                                        </div>
                                        <div class="review-rating">
                                            <ul class="flat-reviews">
                                                <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                                <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                                <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                                <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                                 <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                            </ul>
                                            <p>25 Reviews</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="thumb">
                                            <img src="images/blog/Footer-03.png" alt="image">
                                        </div>
                                        <div class="text">
                                            <a href="#">Software Courses</a>
                                        </div>
                                        <div class="review-rating">
                                            <ul class="flat-reviews">
                                                <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                                <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                                <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                                <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                                 <li class="star">
                                                    <a href="#"><i class="fa fa-star"></i></a>
                                                </li>
                                            </ul>
                                            <p>25 Reviews</p>
                                        </div>
                                    </li>
                                </ul>
                            </div><!-- /.widget-quick-contact -->
                        </div><!-- /.col-md-4-->

                        <div class="col-md-4">
                            <div class="widget widget-quick-contact">
                                <h5 class="widget-title">Quick Contact</h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="flat-contact-form" id="contactform" method="post" action="">

                                            <input type="email" value="" tabindex="2" placeholder="Your Email" name="email" id="email-contact" required="">
                                           
                                            <textarea class="type-input" tabindex="3" placeholder="Message" name="message" id="message-contact" required=""></textarea> 
                                            
                                            <div class="submit-wrap">
                                            <button class="flat-button bg-orange"><i class="fa fa-long-arrow-right"></i></button>
                                            </div>
                                           
                                        </form><!-- /.comment-form -->     
                                    </div><!-- /.col-md-12 -->
                                </div>
                            </div><!-- /.widget .widget-instagram -->
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.footer-widgets -->
        </footer>

        <a class="go-top">
            <i class="fa fa-chevron-up"></i>
        </a>

        <!-- Bottom -->
        <div class="bottom">
            <div class="container">
                <ul class="flat-socials-v1">
                    <li class="facebook">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="twitter">
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="vimeo">
                        <a href="#"><i class="fa fa-vimeo"></i></a>
                    </li>
                    <li class="rss">
                        <a href="#"><i class="fa fa-rss"></i></a>
                    </li>
                </ul>    
                <div class="row">
                    <div class="container-bottom">
                        <div class="copyright"> 
                            <p>Copyright © 2017.<span><a href="#">Leonart advt. Pvt Ltd</a></span>. All Rights Reserved.</p>
                        </div>
                    </div><!-- /.container-bottom -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>