<!-- Start of LiveChat  code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9261585;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<div class="windows8">
            <div class="preload-inner">
                <div class="wBall" id="wBall_1">
                    <div class="wInnerBall"></div>
                </div>
                <div class="wBall" id="wBall_2">
                    <div class="wInnerBall"></div>
                </div>
                <div class="wBall" id="wBall_3">
                    <div class="wInnerBall"></div>
                </div>
                <div class="wBall" id="wBall_4">
                    <div class="wInnerBall"></div>
                </div>
                <div class="wBall" id="wBall_5">
                    <div class="wInnerBall"></div>
                </div>
            </div>
        </div>
    	<div class="header-inner-pages">
    		<div class="top" style="position: absolute;right: 0;left: 0;border: none;z-index: 9;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12" style="padding: 0;">
                            
                            <div class="right-bar" style="border-bottom: 1px solid #2d2d2d;">
                                <ul class="flat-information" style="padding-right: 15px;">
                                  <!--   <li class="phone" style="padding-right: 15px;">
                                        <a href="#" title="Phone number">+91 484 402 2747</a>
                                    </li> -->
                                      <li class="phone" style="padding-right: 15px;">
                                        <a href="#" title="Phone number">+91 884 88326 89</a>
                                    </li>
                                     <li class="phone" style="padding-right: 15px;">
                                        <a href="#" title="Phone number">+91 858 9098 83 </a>
                                    </li>
                                    <li class="email" style="margin-left: 15px;">
                                        <a href="#" title="Email address"> info@educkshetra.com</a>
                                    </li>                                    
                                </ul>
                                <ul class="flat-socials">
                                    <li class="facebook">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="twitter">
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li class="linkedin">
                                        <a href="#">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                    <li class="youtube">
                                        <a href="#">
                                            <i class="fa fa-youtube-play"></i>
                                        </a>
                                    </li>
                                    <li style="margin-left: 2rem;">
                                        <a href="educkshetra-ebrochure.pdf" style="color: white;font-size: 12px;"><i class="fa fa-download" style=" margin-right: 15px;"></i>Download Brochure</a>
                                    </li>
                                    <li><button class="flat-button bg-orange" style="font-size: 10px;margin-left: 35px;background: #65a2eb;border-color: #68a5ed;">Students Login</button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
    	</div><!-- /.header-inner-pages -->

    	<!-- Header --> 
    	<header id="header" class="header clearfix" style="padding-top: 1rem;background: black;"> 
            <div class="container">
                <div class="header-wrap clearfix">
                    <div id="logo" class="logo" style="margin-top: 1em;">
                        <a href="index.php" rel="home">
                            <img src="images/logo.png" alt="image">
                        </a>
                    </div><!-- /.logo -->            
                    <div class="nav-wrap" style="float: right;">
                        <div class="btn-menu">
                            <span></span>
                        </div><!-- //mobile menu button -->
                        <nav id="mainnav" class="mainnav">
                            <ul class="menu"> 
                                <li class="home">
                                    <a href="index.php">Home</a>
                                </li>
                                <li class="has-sub"><a>Courses</a>
                                    <ul class="submenu"> 
                                        <li><a href="edukshetra-courses.php">All Courses</a></li>
                                        <li><a href="edukshetra-courses-networking.php">Networking Courses</a></li> 
                                        <li><a href="edukshetra-courses-software.php">Software Courses</a></li>   
                                        <li><a href="edukshetra-courses-embedded.php">Embedded Courses</a></li> 
                                        <li><a href="edukshetra-digitalmarketing.php">Digital Marketing</a></li>  
                                    </ul><!-- /.submenu -->
                                </li> 

                                <li><a href="edukshetra-about.php">Why Educ kshetra</a>
                                </li>
                                                           

                                <li><a href="edukshetra-academic.php">Academic / Live Projects</a>
                                </li>

                                <li><a href="edukshetra-placements.php">Placements</a>              
                                </li>    

                                <li><a href="edukshetra-contact.php">Contact</a>
                                </li>
                            </ul><!-- /.menu -->
                        </nav><!-- /.mainnav -->    
                    </div><!-- /.nav-wrap -->
                </div><!-- /.header-inner --> 
            </div>
        </header>