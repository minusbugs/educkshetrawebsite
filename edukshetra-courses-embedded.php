<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>educ kshetra - defining careers, transforming lives</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">
	
	<!-- Animation Style -->
    <!-- <link rel="stylesheet" type="text/css" href="stylesheets/animate.css"> -->

  

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head> 
<body class="header-sticky">
<div class="boxed">
     
        <?php
        include ('header.php');
        ?>


        

        <div class="page-title parallax parallax4" style="background-image: url('assets/images/embeded courses.jpg');background-position: center !important;background-size: cover;background-attachment: fixed;"> 
        	<div class="overlay"></div>            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Embedded Courses</h2>
                        </div><!-- /.page-title-heading -->
                        <div class="breadcrumbs">
                            <ul>
                                <li><a>Home</a></li>
                                <li>Courses</li>
                            </ul>                   
                        </div><!-- /.breadcrumbs --> 
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title parallax -->
    	
        <section class="main-content blog-posts flat-row course-grid">
            <div class="container">
                <div class="blog-title clearfix">
                    <h1 class="bold">COURSES</h1>
                </div>
                <div class="row">
                    <div class="flat-post-ordering clearfix">
                        <div class="sort-views">
                            <label class="modern-select">
                                <select name="select_category" class="orderby" id="cat-select">
                                    <option value="0">Select Category</option>
                                    <option value="edukshetra-courses-networking.php">Networking</option>
                                    <option value="edukshetra-courses-software.php">Software</option>
                                    <option value="edukshetra-courses-embedded.php" selected="selected">Embedded</option>
                                </select>
                            </label>

                            <!-- <label class="modern-select">
                                 <select name="select_category" class="orderby">
                                    <option value="menu_order" selected="selected">Sort by</option>
                                    <option value="newest">Newest</option>
                                    <option value="oldest">Oldest</option>
                                </select>
                            </label> -->
                            <div class="list-grid">
                                <a data-layout = "course-grid" class="course-grid-view active" href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
                                <a data-layout = "course-list" class="course-list-view" href="#"><i class="fa fa-list" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="post-warp clearfix courses-edukshetra">

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">The Raspberry Pi is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse. It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python. Our Raspberry Pi workshop has systematically designed so that the students can understand the basics well and can start from the scratch and programming its various features for any applications. Our workshop is purely hands-on and the students can write the code by their-own, test it using hardware.</p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course6.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Raspberry Pi </a> </h4>

                                <div class="price"><span style="font-family: initial;"></span> Embedded</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p>  The Raspberry Pi is a low cost, credit-card sized computerthat plugs into a computer monitor or TV, and uses [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">Arduino is a tool for making computers that can sense and control more of the physical world than your desktop computer. It's an open-source physical computing platform based on a simple microcontroller board, and a development environment for writing software for the board. Our workshop is very systematically designed so that the students can understand the basics well, can do programming of its various peripherals and special features. Our workshop is purely hands-on and the students can write the code by their-own, test it using Simulator and final testing using PIC trainer kits.</p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/arduino.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>Arduino Programming </a> </h4>
                                <div class="price"><span style="font-family: initial;"></span> Embedded</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>25 Reviews</li>
                                </ul> 
                                <p>  Arduino is a tool for making computers that can sense and control more of the physical world than your  [. . .].</p>
                            </div>
                        </div>

                        

                         <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">The internet of things is the internetworking of physical devices, vehicles, buildings and other items—embedded with electronics,software, sensors, actuators, and network connectivity that enable these objects to collect and exchange data. In 2013 the Global Standards Initiative on Internet of Things (IoT-GSI) defined the IoT as "the infrastructure of the information society." The IoT allows objects to be sensed and/or controlled remotely across existing network infrastructure, creating opportunities for more direct integration of the physical world into computer-based systems, and resulting in improved efficiency, accuracy and economic benefit. When IoT is augmented with sensors and actuators, the technology becomes an instance of the more general class of cyber-physical systems, which also encompasses technologies such as smart grids, smart homes, intelligent transportation and smart cities. Each thing is uniquely identifiable through its embedded computing system but is able to interoperate within the existing Internet infrastructure. Experts estimate that the IoT will consist of almost 50 billion objects by 2020.</p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/iot.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Internet of Things </a> </h4>

                                <div class="price"><span style="font-family: initial;"></span> Embedded</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p>  The internet of things is the internetworking of physical devices, vehicles, buildings and other [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        
                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">Printed Circuit Boards (PCB) are inevitable part of any electronics prodiuct. Our workshop is very systematically designed so that the students can understand the basics well, can do programming of its various peripherals and special features. Our workshop is purely hands-on and the students can write the code by their-own, test it using Simulator and final testing using PIC trainer kits.</p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/pcb.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>PCB Fabrication </a> </h4>
                                <div class="price"><span style="font-family: initial;"></span> Embedded</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>25 Reviews</li>
                                </ul> 
                                <p>  Printed Circuit Boards (PCB) are inevitable part of any electronics prodiuct. Our workshop is very systematically [. . .].</p>
                            </div>
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">In engineering education, there are lot of circuits the students need to Familiarize, Learn & Design. The workings of these circuits are learned from books by just reading. But to understand a circuit more, students should see its real time working; see the changes that will happen as design changes etc. By learning Circuit Simulation software, the students can do all the aforesaid steps to analyse their circuits and thereby can make the learning easy & more effective. Our Circuit Simulation Workshop uses Proteus which has both Interactive & Graph based simulation methods.</p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/circuit.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>Circuit Simulation </a> </h4>
                                <div class="price"><span style="font-family: initial;"></span> Embedded</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>25 Reviews</li>
                                </ul> 
                                <p>  In engineering education, there are lot of circuits the students need to Familiarize, Learn & Design.   [. . .].</p>
                            </div>
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">There are some famous wireless technologies which are useful in day to day life. Interfacing these technologies is not at all a tedious task. Clearly understanding its working and associated circuits and output is necessary. Students need to use these for their academic projects and all. So, learning the interfacing of different technologies are necessary. So, we designed this workshop to teach how to interface these technologies with microcontroller</p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/wirelesstech.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Wireless Technology</a> </h4>

                                <div class="price"><span style="font-family: initial;"></span> Embedded</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> There are some famous wireless technologies which are useful in day to day life. Interfacing these technologies  [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                         <p class="course-detail-content hidden">Microcontrollers are an inevitable part of a mixed circuit design. They are ‘the brain of the systems’ and has a huge application in almost all areas like embedded systems, power electronics, modern Electrical systems, instruments etc. Engineers in fields such as electronic, electrical systems, instrumentation etc should learn microcontrollers and its programming for their works to complete. So learning microcontroller programming during the graduation course will be very good for the career of students. And, even the academic projects need some control circuitry. </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/pic.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>PIC Micro controller</a> </h4>

                                <div class="price"><span style="font-family: initial;"></span> Embedded</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> Microcontrollers are an inevitable part of a mixed circuit design. They are ‘the brain   of the systems’ and has a huge [. . .]</p>
                            </div><!-- /.course-content -->
                        </div>

                                  
                    </div><!-- / .post-wrap -->
                </div><!-- /row -->
            </div><!-- /container -->
        </section><!-- /main-content -->


            <?php
            include ("footer.php");
            ?>
        </div> <!-- /.boxed -->

        <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script> 
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
   
    <script type="text/javascript" src="javascript/jquery-validate.js"></script>     
    <script type="text/javascript" src="javascript/main.js"></script>


    <script>
     $('.flat-course').click(function(){
        getImage = $(this).find('img').attr('src');
        getContent = $(this).find('.course-detail-content').html();
        getContentTitle = $(this).find('h4 a').html();
        getContentTag = $(this).find('.price').html();

        $('#course-detail-modal img').attr('src',getImage);
        $('#course-detail-modal .c-content').html(getContent);
        $('#course-detail-modal h4 a').html(getContentTitle);
        $('#course-detail-modal .price').html(getContentTag);


        $('#course-detail-modal').modal('toggle');
     });
    </script>
    <script>
        $('#cat-select').change(function(){
            if($(this).val() != 0){
                window.location.replace($(this).val());
            }
        });
    </script>

</body>
</html>