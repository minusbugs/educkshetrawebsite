<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>educ kshetra - defining careers, transforming lives</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">
	
	<!-- Animation Style -->
    <!-- <link rel="stylesheet" type="text/css" href="stylesheets/animate.css"> -->


    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head> 
<body class="header-sticky">
    <div class="boxed">
        
        <?php
        include ('header.php');
        ?>

        <div class="page-title" style="background-image: url(assets/images/placement.jpg);background-position: center !important;background-size: cover;background-attachment: fixed;"> 
        	<div class="overlay"></div>            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Placements</h2>
                        </div><!-- /.page-title-heading -->
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li>Placements</li>
                            </ul>                   
                        </div><!-- /.breadcrumbs --> 
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title parallax -->
    	
        <!-- About -->
            <section class="flat-row pad-top-100 flat-about">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">


                            <div class="col-md-6">
                                 <div class="text-tab">
                                            <div class="flat-title">
                                                <h1>Careers<span></span></h1>
                                            </div>
                                            <p>We appreciate your interest in exploring career opportunities with us. Educkshetra is a leading training academy and software development centre with a track record of 13 years and provides you with growth opportunities at every step of your career. We give unparalleled opportunities to develop specialized skills and advance their careers.</p>
                                            <p>We are presently looking for:</p>

                                            <ul class="flat-list">  
                                                <li>Technology trainers</li>
                                                <li>Software Developers</li>
                                                <li>Business development Managers</li>
                                            </ul> 
                                            <p>Entry level Freshers in MTech / Btech / MCA / BCA. Educkshetra helps you explore a dynamic career path and a transition from a student to an IT professional We provide continual opportunities to expand on what you can do.</p>
                                            
                                        </div><!-- /.text-tab --> 
                            </div>

                            <div  class="col-md-6">
                                <img src="http://ottawataxiapp.com/wp-content/uploads/2015/02/ottawa-taxi-companies.jpg" alt="images" style="max-width:100%;">
                            </div>


                            <div class="col-md-12" style="margin-top: 2rem;">
                                 <div class="text-tab">
                                            <div class="flat-title">
                                                <h1>Placement<span></span></h1>
                                            </div>
                                            <p>Our courses equips them to rub shoulders with the best IT Professionals and our Placement Cell just provide them the right direction which gives them a flying start. We assist Trainees in benefitting from our Human Resources Data bank since it has built up an enormous database of corporate head hunters/ Reputed Placement agencies. We believe that the success of our courses lies in the success of our students. We also prepare the students with cultural and attitudinal requisites as well.</p>
                                            
                                        </div><!-- /.text-tab --> 
                            </div>



                        </div><!-- /col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->   
            </section>

            <section class="flat-row about-us parallax parallax1">
                <div class="overlay bg-222">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="flat-counter">                            
                                    <div class="counter-content">
                                        <div class="numb-count" data-to="23" data-speed="2000" data-waypoint-active="yes">23</div>
                                        <div class="name-count">Year Of Experience</div>
                                        <!-- <div class="desc-count">Lorem ipsum dolor sit amet consecte tur adipiscing elit, sed do eiusmod tempor incididunt labore</div> -->
                                    </div>
                                </div><!-- /.flat-counter -->
                            </div>

                            <div class="col-md-3 col-sm-6">
                                <div class="flat-counter">                            
                                    <div class="counter-content">
                                        <div class="numb-counter">
                                            <div class="numb-count" data-to="59" data-speed="1000" data-waypoint-active="yes">0</div>
                                        </div>
                                        <div class="name-count">Courses Listed</div>
                                        <!-- <div class="desc-count">Lorem ipsum dolor sit amet consecte tur adipiscing elit, sed do eiusmod tempor incididunt labore</div> -->
                                    </div>
                                </div><!-- /.flat-counter -->
                            </div>

                            <div class="col-md-3 col-sm-6">
                                <div class="flat-counter">                            
                                    <div class="counter-content">
                                        <div class="numb-count" data-to="15" data-speed="2000" data-waypoint-active="yes">15</div>
                                        <div class="name-count">Professional Awards</div>
                                        <!-- <div class="desc-count">Lorem ipsum dolor sit amet consecte tur adipiscing elit, sed do eiusmod tempor incididunt labore</div> -->
                                    </div>
                                </div><!-- /.flat-counter -->
                            </div>

                           <div class="col-md-3 col-sm-6">
                                <div class="flat-counter">                            
                                    <div class="counter-content">
                                        <div class="numb-counter">
                                            <div class="numb-count" data-to="143" data-speed="1000" data-waypoint-active="yes">0</div>
                                        </div>
                                        <div class="name-count">Live Projects</div>
                                        <!-- <div class="desc-count">Lorem ipsum dolor sit amet consecte tur adipiscing elit, sed do eiusmod tempor incididunt labore</div> -->
                                    </div>
                                </div><!-- /.flat-counter -->
                            </div>
                        </div><!-- / .row -->
                    </div><!-- /.container --> 
                </div><!-- /.overlay -->  
            </section>

          

         <section class="flat-row testimonial">
                <div class="container">

                    <div class="flat-title-section">
                        <h1 class="title">TESTIMONIALS</h1>                
                    </div>

                    <div class="testimonial-slider">
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <blockquote>
                                    The trainers are highly skilled and experienced. All being actively involved in the Live projects are enriched with solid experience. 
                                </blockquote>
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-author">
                                    <strong class="author-name">Lima s Sebastian</strong>
                                    <div class="author-info">Btech   ILAHIA College Of Engineering & Technology, Cochin</div>
                                </div>
                            </div>
                        </div>

                        <div class="testimonial">
                            <div class="testimonial-content">
                                <blockquote>
                                    I can confidently say that Educ Kshetra project training center is the answer for all those aspirants who want to become Software developers / Certified Consultants. Also that the target or aim to go for this course is to land up with a better job and career prospects. 
                                </blockquote>
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-author">
                                    <strong class="author-name">Sreeranjini Nandakumar</strong>
                                    <div class="author-info">Btech   Sree Narayana Gurukulam College of Engineering, Cochin</div>
                                </div>
                            </div>
                        </div>

                        <div class="testimonial">
                            <div class="testimonial-content">
                                <blockquote>
                                    I would definitely rejoin and recommend it to other friends and colleagues in the industry who are interested. 
                                </blockquote>
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-author">
                                    <strong class="author-name">Vandana Gopinath</strong>
                                    <div class="author-info">Btech   Indira Gandhi College Of Engineering, Cochin</div>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
            </section>
           

            <?php
            include ("footer.php");
            ?>
    </div><!-- /. boxed -->

   <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script> 
    <script type="text/javascript" src="javascript/owl.carousel.js"></script> 
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/jquery-countTo.js"></script>    
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    
    <script type="text/javascript" src="javascript/jquery-validate.js"></script>     
    <script type="text/javascript" src="javascript/main.js"></script>
</body>
</html>