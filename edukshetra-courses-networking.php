<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>educ kshetra - defining careers, transforming lives</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">
	
	<!-- Animation Style -->
    <!-- <link rel="stylesheet" type="text/css" href="stylesheets/animate.css"> -->

  

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head> 
<body class="header-sticky">
<div class="boxed">
     
        <?php
        include ('header.php');
        ?>

        <div class="page-title" style="background-image: url('assets/images/networking courses.jpg');background-position: center !important;background-size: cover;background-attachment: fixed;"> 
        	<div class="overlay"></div>            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Networking Courses</h2>
                        </div><!-- /.page-title-heading -->
                        <div class="breadcrumbs">
                            <ul>
                                <li><a>Home</a></li>
                                <li>Courses</li>
                            </ul>                   
                        </div><!-- /.breadcrumbs --> 
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title parallax -->
    	
        <section class="main-content blog-posts flat-row course-grid">
            <div class="container">
                <div class="blog-title clearfix">
                    <h1 class="bold">COURSES</h1>
                </div>
                <div class="row">
                    <div class="flat-post-ordering clearfix">
                        <div class="sort-views">
                             <label class="modern-select">
                                <select name="select_category" class="orderby" id="cat-select">
                                    <option value="0">Select Category</option>
                                    <option value="edukshetra-courses-networking.php" selected="selected">Networking</option>
                                    <option value="edukshetra-courses-software.php">Software</option>
                                    <option value="edukshetra-courses-embedded.php">Embedded</option>
                                </select>
                            </label>

                            <!-- <label class="modern-select">
                                 <select name="select_category" class="orderby">
                                    <option value="menu_order" selected="selected">Sort by</option>
                                    <option value="newest">Newest</option>
                                    <option value="oldest">Oldest</option>
                                </select>
                            </label> -->
                            <div class="list-grid">
                                <a data-layout = "course-grid" class="course-grid-view active" href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
                                <a data-layout = "course-list" class="course-list-view" href="#"><i class="fa fa-list" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="post-warp clearfix courses-edukshetra">

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Introduction to Ethical Hacking - Footprinting and Reconnaissance – Scanning Networks – Enumeration – System Hacking – Malware Threats – Evading IDS, Firewalls and Honeypots – Sniffing – Social Engineering – Denial of Service – Session Hijacking – Hacking Web Servers – Hacking Web Applications – SQL Injection – Hacking Wireless Networks – Hacking Mobile Platforms – Cloud Computing – Cryptography
                            </p>

                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course4.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Certified Ethical Hacking </a> </h4>

                                <div class="price"> Networking</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> Introduction to Ethical Hacking - Footprinting and Reconnaissance –Scanning Networks – Enumeration – System Hacking [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                • Operation of IP Data Networks
                                • LAN switching technologies
                                • IP Addressing (IPv4/IPv6)
                                • IP Routing and Technologies
                                • IP services
                                • Network Device Security.WAN Technologist
                            </p>

                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/ccna.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>CCNA</a> </h4>

                                <div class="price"> Networking</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> Operation of IP Data Networks, LAN switching technologies, IP Addressing (IPv4/IPv6), IP Routing and Technologies [. . .]</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                CCNP (Cisco Certified Network Professional)
                                     • Function of a Switch
                                     • Switching operation
                                     • L2 switch operations
                                     • Multi-layer switching
                                     • VLANs and Trunks
                                     • Dot1Q, ISL, DTP
                                     • Inter VLAN routing
                                     • VTP, VTP Pruning
                                     • Link Aggregation-LACP PAgP
                                     • STP-CSTP, PVSTP, RSTP MSTP
                                     • High availability
                                     • First hope redundancy protocol
                                     • HSRP, VSRP
                                     • Campus network design
                                     • IP Telephony
                                     • Switch maintenance and security
                                     • CCNP Route
                                     • Summarisation, Authentication,
                                     • Filtering, Tagging and redistribution of RIP
                                     • Summarisation, Authentication, Filtering,
                                     • Tagging and redistribution of EIGRP
                                     • Stub networks in EIGRP
                                     • Summarisation, Authentication, Filtering,
                                     • Tagging and redistribution of OSPF Protocol
                                     • LSA and LSA types
                                     • Stub network in OSPF
                                     • Understanding TSA and NSSA
                                     • Advance routing, Advance
                                     • redistribution Policy based routing,
                                     • BGP Protocol, EGBP Properties, IBGP
                                     • Synchronisation Confederation and
                                     • router reflector, BGP Community, BGP
                                     • attributes, Route aggregation, Route
                                     • Filtering, IPv6, Neighbor discovery
                                     • protocol, IPv6 dynamic routing with
                                     • RIPng, OSPF v3 and EIGRP
                                     • IPv6 transition from IPv4.
                            </p>

                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/ccnp.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>CCNP</a> </h4>

                                <div class="price"> Networking</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> Function of a Switch, Switching operation, L2 switch operations, Multi-layer switching, VLANs and Trunks, Dot1Q [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Deploying and Managing Windows Server 2012 – Active Directory Domain Services – User and Service Accounts – DHCP – DNS – Group Policy – Local Storage – Server Virtualisation with Hyper – V –VPN – Distributed File System – AD DS Sites and Replication – Network and Load Balancing – File and Print Services – Hyper – V Live Migrations
                            </p>

                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/mcse.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>MCSE</a> </h4>

                                <div class="price"> Networking</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> Deploying and Managing Windows Server 2012 – Active Directory Domain Services – User and Service Accounts  [. . .]</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                General Networking Theory | General Routing Concepts | Routing Information Base(RIB) and Routing Protocols Interaction | Redistribution | Bridging and LAN Switching | Spanning Tree Protocol (STP) | LAN Switching | IP | Addressing | Services | Network Management | IP Routing | OSPF | BGP | EIGRP | Policy Routing | QoS | WAN | IP Multicast | Security | MPLS (New) | IPv6 (New)
                            </p>

                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/ccie.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>CCIE</a> </h4>

                                <div class="price"> Networking</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> General Networking Theory | General Routing Concepts | Routing Information Base(RIB) and Routing Protocols [. . .]</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Configuring static routes, packet filtering, and network address translation | Setting kernel runtime parameters | Configuring an Internet Small Computer System Interface (iSCSI) initiator | Producing and delivering reports on system utilization | Using shell scripting to automate system maintenance tasks | Configuring system logging, including remote logging | Configuring a system to provide networking services, including HTTP/HTTPS, file Transfer Protocol (FTP), network file system (NFS), server message block (SMB), Simple Mail Transfer Protocol (SMTP), secure shell (SSH) and Network Time Protocol (NTP)
                            </p>

                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/rhce.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>RHCE </a> </h4>

                                <div class="price"> Networking</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> Configuring static routes, packet filtering, and network address translation | Setting kernel runtime parameters [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>



                        
                    </div><!-- / .post-wrap -->
                </div><!-- /row -->
            </div><!-- /container -->
        </section><!-- /main-content -->


            <?php
            include ("footer.php");
            ?>
        </div> <!-- /.boxed -->

        <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script> 
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
   
    <script type="text/javascript" src="javascript/jquery-validate.js"></script>     
    <script type="text/javascript" src="javascript/main.js"></script>

    <script>
     $('.flat-course').click(function(){
        getImage = $(this).find('img').attr('src');
        getContent = $(this).find('.course-detail-content').html();
        getContentTitle = $(this).find('h4 a').html();
        getContentTag = $(this).find('.price').html();

        $('#course-detail-modal img').attr('src',getImage);
        $('#course-detail-modal .c-content').html(getContent);
        $('#course-detail-modal h4 a').html(getContentTitle);
        $('#course-detail-modal .price').html(getContentTag);


        $('#course-detail-modal').modal('toggle');
     });
    </script>
    <script>
        $('#cat-select').change(function(){
            if($(this).val() != 0){
                window.location.replace($(this).val());
            }
        });
    </script>
</body>
</html>