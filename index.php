<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>educ kshetra - defining careers, transforming lives</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/shortcodes.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">


    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">

    <script src="https://use.fontawesome.com/2d4b939c03.js"></script>
	
	<!-- Animation Style -->
    <!-- <link rel="stylesheet" type="text/css" href="stylesheets/animate.css"> -->


    <link rel="stylesheet" href="anim/css/custom/style.css">

   

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->

    <style>
    .carousel-indicators .active{
        background-color: #0088ae;
    }
    .carousel-indicators li{
        border-color: #0088ae;
    }

.tp-caption.desc-slide.center, .tp-caption.title-slide.center {
    margin-top: -2rem !important;
}

.tp-caption.sfl.title-slide{
    line-height: 20px !important;
}

.tp-caption.flat-button-slider.bg-orange {
    margin-top: -2rem !important;
}

.tp-caption.sfl.title-slide {
    line-height: 60px !important;
    margin-top: -7rem !important;
}

.tp-caption.sfl.flat-button-slider.bg-orange
{
    margin-left: 5rem !important;
}


.round_animate {
    width: 300px;
    height: 300px;
    position: relative;
    margin: 40px auto;
}

    </style>

</head> 
<body class="header-sticky">
    <div class="boxed">
        
        <?php
        include ('header.php');
        ?>

<div id="edu-banner" class="carousel slide" data-ride="carousel">



        <!-- Slider -->
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>

                         <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                            <img src="images/slides/digital.jpg" alt="slider-image" />
                           <!--  <div class="tp-caption sfl title-slide center" data-x="174" data-y="250" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                               POINTING TOWARDS<br>THE RIGHT CAREERS
                            </div>  
                            <div class="tp-caption sfr desc-slide center" data-x="256" data-y="310" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">                       
                               Of course you need to focus on what you choose.<br>Here we help you choose the right career according to your skill set.
                            </div>  -->   
                            <div class="tp-caption sfl flat-button-slider bg-orange" data-x="420" data-y="439" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut" style="border-color: #00BCD4;background: #00BCD4;"><a class="" href="edukshetra-courses.php">START A COURSE</a></div>
                             <!-- <div class="tp-caption sfr flat-button-slider" data-x="601" data-y="440" data-speed="1000" data-start="2500" data-easing="Power3.easeInOut"><a class="" href="edukshetra-about.php">TAKE A TOUR</a></div> -->                    
                        </li>

                        <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                            <img src="images/slides/slide2.jpg" alt="slider-image" />
                            <div class="tp-caption sfl title-slide center" data-x="174" data-y="250" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">      	                      
                               CLIMB UP THE<br>LADDER
                            </div>  
                            <div class="tp-caption sfr desc-slide center" data-x="256" data-y="310" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">                       
                                Educ kshetra act as a catalyst for achieving your career goals.    
                            </div>    
                            <div class="tp-caption sfl flat-button-slider bg-orange" data-x="420" data-y="439" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut" style="border-color: #00BCD4;background: #00BCD4;"><a class="" href="edukshetra-courses.php">START A COURSE</a></div>
                             <!-- <div class="tp-caption sfr flat-button-slider" data-x="601" data-y="440" data-speed="1000" data-start="2500" data-easing="Power3.easeInOut"><a class="" href="edukshetra-about.php">TAKE A TOUR</a></div> -->                    
                        </li>
                        
                       

                        <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                            <img src="images/slides/slide3.jpg" alt="slider-image" />
                            <div class="tp-caption sfl title-slide center" data-x="74" data-y="250" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                               SHIELDS YOU AGAINST<br>COMPETITION
                            </div>  
                            <div class="tp-caption sfr desc-slide center" data-x="256" data-y="310" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">                       
                                Right now to shine in your trajectory you need to perform well.<br>Here we mentor you to survive the competition.
                            </div>    
                            <div class="tp-caption sfl flat-button-slider bg-orange" data-x="420" data-y="439" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut" style="border-color: #00BCD4;background: #00BCD4;"><a class="" href="edukshetra-courses.php">START A COURSE</a></div>
                             <!-- <div class="tp-caption sfr flat-button-slider" data-x="601" data-y="440" data-speed="1000" data-start="2500" data-easing="Power3.easeInOut"><a class="" href="edukshetra-about.php">TAKE A TOUR</a></div> -->                    
                        </li>

                    </ul>
                </div>
            </div><!-- /.tp-banner-container -->

            <div class="flat-row course row-bg" style="padding-bottom: 0;">
                <div class = "container">


                        <div class="round_animate_wrap" style="left: 0;margin: -5em; z-index: 1;">
                            <div class="round_animate">
                                <div class="shape_1">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                                <div class="shape_2">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                                <div class="shape_3">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                            </div>
                        </div>


                    <div class="row">
                        <div class="col-md-7 col-sm-12 flat-pdr-100">                       
                            <h1 class="title-course">Strives for excellence in teaching and learning</h1>
                            <p class='flat-lh-25'>
                                Educ kshetra, aptly named, has its moorings in Tripunithura, the satellite temple town of Kochi and is committed to providing high quality IT Training in the cutting edge technologies like Anroid, Asp.Net Java J2EE Php Sharepoint and more. Best for MTech BTech MCA BCA BSc.Computer Science Diploma holders and also IT professionals.
                            </p> 

                            <div class="flat-spacer"></div>  

                            <div class="flat-button-container">
                                <a class="flat-button orange" href="edukshetra-courses.php">VIEW ALL COURES</a>
                            </div> 
                        </div>

                        <div class="col-md-5 col-sm-12"> 
                            <ul class = "flat-course-images">
                                <li>
                                    <img src="images/index/c1.jpg" alt="c1"/>
                                </li>

                                <li>
                                    <img src="images/index/c2.jpg" alt="c2"/>
                                </li>

                                <li>
                                    <img src="images/index/c3.jpg" alt="c3"/>
                                </li>

                                <li>
                                    <img src="images/index/c4.jpg" alt="c4"/>
                                </li>

                                <li>
                                    <img src="images/index/c5.jpg" alt="c5"/>
                                </li>

                                <!-- <li>
                                    <img src="images/index/c6.jpg" alt="c6"/>
                                </li> -->
                            </ul>  
                        </div>                    

                    </div>
                </div>
            </div><!-- /.flat-row -->

            <div class = " flat-row popular-course">
                <div class="container">
                    <div class="flat-title-section">
                        <h1 class="title">POPULAR COURSES</h1>                
                    </div>

                    <div class="flat-course-grid button-right">
                        <div class="flat-course">
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a href="edukshetra-courses.php"><img src="images/index/course1.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a href="edukshetra-courses.php">Angular JS</a> </h4>

                                <div class="price"><span style="font-family: initial;"></span> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> Introduction to AngularJS. Understanding AngularJS Blocks. AngularJS - MVC Architecture.AngularJS Modules, Forms [. . .].</p>

                                <!-- <ul class="course-meta desc">
                                    <li>
                                        <h6>1 year</h6>
                                        <span> Course</span>
                                    </li>

                                    <li>
                                        <h6>25</h6>
                                        <span> Class Size</span>
                                    </li>

                                    <li>
                                        <h6><span class="course-time">7:00 - 10:00</span></h6>
                                        <span> Class Duration</span>
                                    </li>
                                </ul>  -->
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course">
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a href="edukshetra-courses.php"><img src="images/index/course2.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a href="edukshetra-courses.php">Android App Development</a> </h4>

                                <div class="price"><span style="font-family: initial;"></span> Mobile</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> JAVA Concepts, Introduction to Android, Application Structure (in detail), Emulator-Android Virtual Device [. . .]</p>

                        
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course">
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a href="edukshetra-courses.php"><img src="images/index/course3.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a href="edukshetra-courses.php">Advanced HTML5</a> </h4>

                                <div class="price"><span style="font-family: initial;"></span> Web</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> Raw HTML5 +CSS3, HTML5 Canvas, Adobe Edge, Mobile Web and HTML5, PHP & MySQL Course Curriculum Introduction [. . .]</p>

                          
                            </div><!-- /.course-content -->
                        </div>
                    </div><!-- /.flat-course grid -->



                        <div class="round_animate_wrap" style="left: 0;margin: 20em;z-index: 0;right: 0;">
                            <div class="round_animate">
                                <div class="shape_1">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                                <div class="shape_2">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                                <div class="shape_3">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                            </div>
                        </div>



                </div>
            </div>

            <div class="flat-row join-us parallax parallax1 overlay bg-222">
                <div class="overlay bg-222">
                    <div class="container">
                        <div class="row">

                            <div class="counter-content">
                                <span class="counter-prefix">Join</span>
                                <div class="numb-counter">
                                    <div class="numb-count" data-to="112093" data-speed="1000" data-waypoint-active="yes">0</div>
                                </div>          
                                <span class="counter-suffix">people</span>
                            </div>

                            <p>Where knowledge is honed to high skill with quality training in information technology. This ensures our students or your workforce are industry ready and tuned into organisational goals</p>

                            <a href="edukshetra-contact.php" class="flat-button">JOIN NOW</a>
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div>
            </div>

            <section class="flat-row testimonial">
                <div class="container">

                    <div class="flat-title-section">
                        <h1 class="title">TESTIMONIALS</h1>                
                    </div>


                        <div class="round_animate_wrap" style="margin: 0em;z-index: 0;">
                            <div class="round_animate">
                                <div class="shape_1">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                                <div class="shape_2">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                                <div class="shape_3">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                            </div>
                        </div>

                    <div class="testimonial-slider">
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <blockquote>
                                    The trainers are highly skilled and experienced. All being actively involved in the Live projects are enriched with solid experience. 
                                </blockquote>
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-author">
                                    <strong class="author-name">Lima s Sebastian</strong>
                                    <div class="author-info">Btech   ILAHIA College Of Engineering & Technology, Cochin</div>
                                </div>
                            </div>
                        </div>

                        <div class="testimonial">
                            <div class="testimonial-content">
                                <blockquote>
                                    I can confidently say that Educ Kshetra project training center is the answer for all those aspirants who want to become Software developers / Certified Consultants. Also that the target or aim to go for this course is to land up with a better job and career prospects. 
                                </blockquote>
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-author">
                                    <strong class="author-name">Sreeranjini Nandakumar</strong>
                                    <div class="author-info">Btech   Sree Narayana Gurukulam College of Engineering, Cochin</div>
                                </div>
                            </div>
                        </div>

                        <div class="testimonial">
                            <div class="testimonial-content">
                                <blockquote>
                                    I would definitely rejoin and recommend it to other friends and colleagues in the industry who are interested. 
                                </blockquote>
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-author">
                                    <strong class="author-name">Vandana Gopinath</strong>
                                    <div class="author-info">Btech   Indira Gandhi College Of Engineering, Cochin</div>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
            </section>

            <section class="flat-row news-letter" style="padding-top: 0;">
                <div class="container">
                    <div class="have-question">                       
                        <h2 class="">Have any question for us?</h2>

                        <p class="flat-lh-28">
                            Workshop, events & other activities to encourage student-industry interaction.
                        </p> 

                        <div class="flat-button-container">
                            <a class="read-more" href="edukshetra-contact.php">Contat Us</a>
                        </div> 
                    </div>



                        <div class="round_animate_wrap" style="margin: -10em;z-index: 0;left: 0;">
                            <div class="round_animate">
                                <div class="shape_1">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                                <div class="shape_2">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                                <div class="shape_3">
                                    <span class="r_1"></span>
                                    <span class="r_2"></span>
                                    <span class="r_3"></span>
                                    <span class="r_4"></span>
                                </div>
                            </div>
                        </div>



                    <div class="news-letter-form">
                        <div class="widget-mailchimb">
                            <h1 class="widget-title">NEWSLETTER</h1>
                            <p>Subscribe now and receive weekly newsletter with educational materials, new courses, interesting posts, popular books and much more!</p>
                            <form method="post" action="#" id="subscribe-form" data-mailchimp="true">
                                    <div id="subscribe-content">
                                        <div class="input-wrap email">
                                            <input type="text" id="subscribe-email" name="subscribe-email" placeholder="Your Email Here">
                                        </div>
                                        <div class="button-wrap">
                                            <button type="button" id="subscribe-button" class="subscribe-button" title="Subscribe now"> SUBSCRIBE </button>
                                        </div>
                                    </div>
                                    <div id="subscribe-msg"></div>
                                </form>
                           
                        </div>
                    </div>
                </div>
            </section>
            
            <?php
            include ("footer.php");
            ?>

    </div><!-- /. boxed -->


        <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script> 
    <script type="text/javascript" src="javascript/owl.carousel.js"></script> 
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/jquery-countTo.js"></script>    
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/jquery-validate.js"></script>     
    <script type="text/javascript" src="javascript/main.js"></script>

	<!-- Revolution Slider -->
    <script type="text/javascript" src="javascript/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="javascript/slider.js"></script>

</body>
</html>