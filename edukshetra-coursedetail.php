<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>educ kshetra - defining careers, transforming lives</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">
	
	<!-- Animation Style -->
    <!-- <link rel="stylesheet" type="text/css" href="stylesheets/animate.css"> -->

   

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head> 
<body class="header-sticky">
    <div class="boxed">
        
        <?php
        include ('header.php');
        ?>

        <div class="page-title parallax parallax4" style="background-image: url(http://www.mindinventory.com/blog/wp-content/uploads/2017/02/angularjs-development.jpg);"> 
        	<div class="overlay"></div>            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Angular JS</h2>
                        </div><!-- /.page-title-heading -->
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Courses</a></li>
                                <li>Course Details</li>
                            </ul>                   
                        </div><!-- /.breadcrumbs --> 
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title parallax -->
    	
        <section class="main-content blog-posts course-single">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="blog-title-single">
                            <!-- <h1 class="bold">Angular Js <span>Software</span></h1>
                            <div class="feature-post">
                                <img src="images/courses/angular.jpg" alt="image">
                            </div> --><!-- /.feature-post -->
                            
                            <div class="entry-content">                                
                                <img src="images/index/course1.jpg" class="img-responsive" style="display: inline-block;float: left;margin-right: 20px;">
                                <h4 class="title-1 bold">ABOUT THE COURSES</h4>
                                <p>
                                   
                                </p> 

                                <div class="flat-spacer h8px"></div>
                                <h4 class="title-2">What You Will Learn</h4>
                                <p>
                                   Introduction to AngularJS. Understanding AngularJS Blocks. AngularJS - MVC Architecture. AngularJS Modules, Forms, etc… Ajax in AngularJS. AngularJS Services. Working with AngularJS. In Web Application Development. AngularJS Applications
                                </p>                               
                               

                               
                                <a class="flat-button bg-orange" href="#">ENROLL THIS COURSE</a>
                                
                            </div><!-- /.entry-post -->


                            <div class="col-md-12 widget">
                                <h5 class="widget-title">Similar courses</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="flat-course flat-hover-zoom">
                                    <div class="featured-post">             
                                        <div class="overlay">
                                            <div class="link"></div>
                                        </div>
                                        <a href="#"><img src="images/index/ios.jpg" alt="Course1"></a>
                                    </div>
                                    <div class="course-content">
                                        <h4><a href="#">iOS </a> </h4>
                                        <div class="price"><span style="font-family: initial;"></span> Mobile</div>
                                        <ul class="course-meta review">
                                            <li class="review-stars">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half"></i>
                                                <i class="fa fa-star-o"></i>
                                            </li>
                                            <li>25 Reviews</li>
                                        </ul> 
                                        <p>  Fundamentals- OOPS, Software Engineering, SQL Queries, Basics of Designing. Objective CData Type, NSInterger [. . .].</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="flat-course flat-hover-zoom">
                                    <div class="featured-post">             
                                        <div class="overlay">
                                            <div class="link"></div>
                                        </div>
                                        <a href="#"><img src="images/index/bootstrap.jpg" alt="Course1"></a>
                                    </div>
                                    <div class="course-content">
                                        <h4><a href="#">Bootstrap </a> </h4>
                                        <div class="price"><span style="font-family: initial;"></span> Web</div>
                                        <ul class="course-meta review">
                                            <li class="review-stars">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half"></i>
                                                <i class="fa fa-star-o"></i>
                                            </li>
                                            <li>25 Reviews</li>
                                        </ul> 
                                        <p>  Introduction to Bootstrap. Responsive web page design using Grid System.Implementing Bootstrap with CSS Classes. [. . .].</p>
                                    </div>
                                </div>
                            </div>


                          

                        </div><!-- /.main-post -->

                       
                    </div><!-- /col-md-8 -->

                    <div class="sidebar">
                        <div class="widget widget-categories">
                            <h5 class="widget-title">Categories</h5>
                            <ul>
                                <li>
                                    <a href="#">Networking</a>
                                    <span class="numb-right">(6)</span>
                                </li>
                                <li>
                                    <a href="#">Software</a>
                                    <span class="numb-right">(14)</span>
                                </li>
                                <li>
                                    <a href="#">Embedded</a>
                                    <span class="numb-right">(8)</span>
                                </li>
                            </ul>
                        </div>

                    
                        <div class="widget widget-featured-courses">
                            <h5 class="widget-title">Featured courses</h5>
                            <ul class="featured-courses-news clearfix">
                                <li>
                                    <!-- <div class="thumb">
                                        <img src="images/flickr/7.jpg" alt="image">
                                    </div> -->
                                    <div class="text">
                                        <a href="#">Certified Ethical Hacking</a>
                                        <p>Networking</p>
                                    </div>
                                    <div class="review-rating">
                                        <div class="flat-money">
                                            <p>View</p>
                                        </div>
                                        <ul class="flat-reviews">
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                            </li>
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                            </li>
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                            </li>
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star-half-o"></i></a>
                                            </li>
                                             <li class="star">
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li>
                                    <!-- <div class="thumb">
                                        <img src="images/flickr/7.jpg" alt="image">
                                    </div> -->
                                    <div class="text">
                                        <a href="#">Android App Development</a>
                                        <p>Software</p>
                                    </div>
                                    <div class="review-rating">
                                        <div class="flat-money">
                                            <p>View</p>
                                        </div>
                                        <ul class="flat-reviews">
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                            </li>
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                            </li>
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                            </li>
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star-half-o"></i></a>
                                            </li>
                                             <li class="star">
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li>
                                    <!-- <div class="thumb">
                                        <img src="images/flickr/7.jpg" alt="image">
                                    </div> -->
                                    <div class="text">
                                        <a href="#">Advanced HTML5 & CSS3</a>
                                        <p>Web</p>
                                    </div>
                                    <div class="review-rating">
                                        <div class="flat-money">
                                            <p>View</p>
                                        </div>
                                        <ul class="flat-reviews">
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                            </li>
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                            </li>
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star"></i></a>
                                            </li>
                                            <li class="star">
                                                <a href="#"><i class="fa fa-star-half-o"></i></a>
                                            </li>
                                             <li class="star">
                                                <a href="#"><i class="fa fa-star-o"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                
                            </ul><!-- /popular-news clearfix -->
                        </div><!-- /widget widget-popular-news -->
                    </div><!-- /sidebar -->
                </div><!-- /row -->
            </div><!-- /container -->
        </section><!-- /main-content -->

            <?php
            include ("footer.php");
            ?>
    </div> <!-- /.boxed -->

        <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script> 
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>

    <script type="text/javascript" src="javascript/jquery-validate.js"></script>     
    <script type="text/javascript" src="javascript/main.js"></script>

</body>
</html>