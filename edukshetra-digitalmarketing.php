<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
   <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]>
         <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
         <![endif]-->
    <title>educ kshetra - defining careers, transforming lives</title>
    <meta name="author" content="themesflat.com">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css">
    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">
    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">
    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">
    <!-- Animation Style -->
    <!-- <link rel="stylesheet" type="text/css" href="stylesheets/animate.css"> -->
    <!--[if lt IE 9]>
         <script src="javascript/html5shiv.js"></script>
         <script src="javascript/respond.min.js"></script>
         <![endif]-->
</head>

<body class="header-sticky">
    <div class="boxed">
        <?php
            include ('header.php');
            ?>
            <div class="page-title" style="background-image: url(assets/images/digital-marketing.jpg);background-position: center !important;background-size: cover;background-attachment: fixed;">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title-heading">
                                <h2 class="title">Digital Marketing Training Program</h2>
                            </div>
                            <!-- /.page-title-heading -->
                            <div class="breadcrumbs">
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Courses</a></li>
                                    <li>Digital Marketing</li>
                                </ul>
                            </div>
                            <!-- /.breadcrumbs -->
                        </div>
                        <!-- /.col-md-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /page-title parallax -->
            <!-- About -->
            <section class="main-content blog-posts course-single" style="padding-bottom: 0;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="blog-title-single">
                                <div class="row">
                                     <div class="col-md-6">
                                        <h1 class="bold">OVERVIEW</h1>
                                     </div>
                                     <div class="col-md-6">
                                        <a href="#" class="flat-button" data-toggle="modal" data-target="#Download-Curriculum-modal">Download Curriculum</a>
                                     </div>
                                </div>
                                <div class="row">
                                    <div class="entry-content">
                                    
                                        <p>
                                            This course is designed to help you understand the various disciplines in digital marketing and how to apply the in depth knowledge gained from its various facets into your business to increase your brand awareness and to ensure the product sales increase with minimum investment.
                                        </p>
                                        <div class="flat-spacer h8px"></div>
                                        <h4 class="title-1 bold" style="text-transform: uppercase;">What You Will Learn</h4>
                                        <!-- <h4 class="title-2">What You Will Learn</h4> -->
                                        <p>The program is developed by industry experts keeping in mind that in the current industry scenario, the skill to apply your knowledge is more important than gaining knowledge. Our curriculum helps students to attain practical knowledge as the sessions are 70 percent practical and only 30 percent is theory.</p>
                                        <p>This program is ideally for business owners who want to increase their online business and sales revenue or for marketing professionals who want to improve their digital marketing skills for a successful career and for job seekers who want to make a career in digital marketing.</p>
                                        <p>Digital Marketing is a booming industry and there is a plethora of job opportunities available for skilled candidates. The program would help the participants not only to gain indepth knowledge but also become an expert in digital marketing.</p>
                                    </div>
                                </div>
                                                               
                                 
                                
                                <!-- /.entry-post -->
                            </div>
                            <!-- /.main-post -->
                        </div>
                        <div class="col-md-5">
                            <img src="assets/images/digital-marketing_01.png" style="   width: 100%;">
                        </div>

                        <div class=" flat-row popular-course">
                            <div class="container">

                                <div class="flat-course-grid button-right">
                                    <div class="flat-course">
                                        <div class="featured-post">
                                            <div class="overlay">
                                                <div class="link"></div>
                                            </div>

                                            <a><img src="images/digital-certf.jpg" alt="Course1"></a>
                                        </div>
                                        <!-- /.featured-post -->

                                        <div class="course-content">
                                            <h4><a>GET CERTIFIED</a> </h4>
                                            <p> We will guide you in attaining the industry leading certification for digital marketing from google
                                                <br>
                                                <br>
                                            </p>
                                        </div>
                                        <!-- /.course-content -->
                                    </div>

                                    <div class="flat-course">
                                        <div class="featured-post">
                                            <div class="overlay">
                                                <div class="link"></div>
                                            </div>

                                            <a><img src="images/digital-experienced.jpg" alt="Course1"></a>
                                        </div>
                                        <!-- /.featured-post -->

                                        <div class="course-content">
                                            <h4><a>GET EXPERIENCED</a> </h4>
                                            <p>We have our trainees undergo a hands on training approach that consists only 20% of theory while the rest is taken up as real-time practical training</p>
                                        </div>
                                        <!-- /.course-content -->
                                    </div>

                                    <div class="flat-course">
                                        <div class="featured-post">
                                            <div class="overlay">
                                                <div class="link"></div>
                                            </div>

                                            <a><img src="images/digital-placed.jpg" alt="Course1"></a>
                                        </div>
                                        <!-- /.featured-post -->

                                        <div class="course-content">
                                            <h4><a>GET PLACED</a> </h4>
                                            <p>Having equipped the trainee with the right skill-set, we help them find their dream job.
                                                <br>
                                                <br>
                                                <br>
                                            </p>
                                        </div>
                                        <!-- /.course-content -->
                                    </div>

                                </div>
                                <!-- /.flat-course grid -->
                            </div>
                        </div>
                        <div style="padding: 35px 0px;background-color: #fdfdfd;">
                            <div class="container" style=" background: #fdfdfd;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <h2 class="bold mgbt-17" style="text-transform: uppercase;text-align: center;">Core Modules</h2> -->
                                        <h4 class="bold mgbt-17" style="text-transform: uppercase;">Core Modules</h4>
                                        <!-- <p style="text-align: center;">An insight into digital marketing and how it’s different from traditional marketing. </p> -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="flat-accordion">
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Introduction to Digital Marketing</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>3 hrs</b></h5>
                                                        <p> An insight into digital marketing and how it’s different from traditional marketing. Understanding the different ways to boost your business with digital marketing. This module covers topics such as</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> What is Digital Marketing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Understanding Digital Marketing Proccess</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Increasing visibility</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Visitors Engagement</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Bringing Targeted Traffic</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Converting traffic into leads</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Retention</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Performance Evaluation</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Website Creation</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>6 hrs</b></h5>
                                                        <p> Learn how to create an engaging website for your business. During the course of this module, you will learn how to buy a domain name and web hosting. How to add webpages and content. The topics covered will be</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Understanding domain names and domain extensions</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Adding web pages and content </li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Building a website</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Email Marketing </h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>3 hrs</b></h5>
                                                        <p> How to send bulk email and generate leads and convert to sales with email marketing. Understand what is email marketing,</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Types of email marketing-Opt-in & Bulk emailing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Creating email marketing account</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>How to do bulk emailing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>What are auto responders</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>The ways to ensure the email lands in inbox instead of spam folder</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Lead Generation</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>3 hrs</b></h5>
                                                        <p> Learn how to generate and Convert leads into sales by creating landing pages The module will help you understand </p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How to nurture the leads</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Converting leads into sales</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Understanding landing pages</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Understanding Thank you page</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How to do A/B testing</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Google Adwords</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>12 hrs</b></h5>
                                                        <p> A deep understanding of how ad word works and different tools that can make your advertising strategy more effective. The module will help you understand the skill and knowledge required to run an effective marketing campaigns. The module will cover the following topics.</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Introduction to Google Adwords</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Setting up google adword account</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Understanding Adwords Algorithm</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Creating Search Campaigns</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Understanding key words</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Creating Ads</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Optimizing search campaigns</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Creating display campaigns</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Optimizing display campaigns</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Remarketing</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Search Engine Optimization ( SEO)</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>12 hrs</b></h5>
                                                        <p> Learn the rules to be followed to optimize the visibility of your website and ensuring to bring it to the top page of google search engine. In this you will learn the various tools and techniques to be used to optimize the visibility of your website. Below are the topics that would be covered in this module.</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> What is SEO</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> What are search engines</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How search Engines work</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Different types of keywords</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Google key word planner tool</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Understanding keywords mix</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> On page optimization</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                        </div>
                                        <!-- /.flat-accordion -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="flat-accordion">
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Social Media Marketing ( SMM)</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>12 hrs</b></h5>
                                                        <p> How to use social media platforms to promote your product. How to get connected to the customers on social media like Instagram, Facebook, Twitter and youtube. The topics covered are</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Forms of internet marketing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>What are the smm platforms</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Facebook advertising</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Types of facebook advertising</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Create face book advertising campaign</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Twitter Marketing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>How to do marketing on twitter</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Creating Campaigns</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Video Marketing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Understanding video campaign</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Using youtube for business</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span>Creating video Adgroups</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Google Analytics</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>3 hrs</b></h5>
                                                        <p> Monitor your business with the help of Google Analytics. It lets you measure, compare and perform better in your ad campaign.</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Introduction to Google Analytics</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How it works</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> setup google analytics account to create a measurement model</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How to integrate analytic code to website</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Measuring performance of marketing campaigns</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Content Marketing </h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>3 hrs</b></h5>
                                                        <p> Learn the art of writing content to optimise your product visibility. This module will help you understand the importance of having a good content for your website and how it can attract your targeted audience</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> What is Content Marketing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Objectives of Content Marketing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How to write great content</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Different ways to market your content</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Ways to connect with your consumers through content</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Case study on content marketing</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Ecommerce Marketing</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>3 hrs</b></h5>
                                                        <p> This module contains</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Learn to sell products on ecommerce websites</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> What is ecommerce </li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How to do SEO for ecommerce website</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Setting up an ecommerce store on wordpress</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Case studies on Ecommerce websites</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Mobile Marketing</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>3 hrs</b></h5>
                                                        <p> Understanding the basics of mobile marketing. You will learn to create a mobile website through workdpress. In this three hour session, you will learn the following:</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Using tools to create Mobile websites</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Advertising on mobile app</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> SMS Marketing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Content Marketing</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How to launch mobile campaigns to boost your sales</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                            <div class="flat-toggle">
                                                <h6 class="toggle-title">Adsense &amp; Blogging</h6>
                                                <div class="toggle-content" style="display: none;">
                                                    <div>
                                                        <h5 style="border-top: 1px solid #dedede;border-bottom: 1px solid #dedede;padding: 10px 0px;margin-bottom: 15px;">Duration: <b>3 hrs</b></h5>
                                                        <p> Learn the ways to get your website adsense approved. Understand what is adsense and how to get the adsense approved</p>
                                                        <ul style="margin-left: 15px;">
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> What is Adsense</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How to get approval for adsense</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> Placing ads on your blog</li>
                                                            <li><span class="fa fa-circle" style="margin-right: 10px;color: #adacac;"></span> How to make money with Adsense</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /toggle -->
                                        </div>
                                        <!-- /.flat-accordion -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4 class="bold mgbt-17" style="text-transform: uppercase;">Training Programs</h4>
                            <!-- <p class="mgbt-26">
                        An insight into digital marketing and how it’s different from traditional marketing. Understanding the different ways to boost your business with digital marketing. 
                        </p>   -->
                            <ul class="curriculum col-md-6" style="padding-left: 0;">
                                <li class="section">
                                    <h6 class="section-title" style="font-weight: 600;background: #eaeaea;padding: 25px 20px;">Fast Track Batch- 40 hours<span style="font-weight: 100;    float: right;">7  days</span></h6>
                                    <ul class="section-content">
                                        <li>
                                            <h5 style="margin-bottom: 5px;font-weight: 800;">Day 1</h5>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">An overview of Digital Marketing</a>
                                            <div class="fr">
                                                <span class="duration">1 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Lead Generation</a>
                                            <div class="fr">
                                                <span class="duration">2 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Email Marketing</a>
                                            <div class="fr">
                                                <span class="duration">2 hour</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="section-content">
                                        <li>
                                            <h5 style="margin-bottom: 5px;font-weight: 800;">Day 2</h5>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Google Adwords</a>
                                            <div class="fr">
                                                <span class="duration">6 hour</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="section-content">
                                        <li>
                                            <h5 style="margin-bottom: 5px;font-weight: 800;">Day 3</h5>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Google Adwords</a>
                                            <div class="fr">
                                                <span class="duration">6 hour</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="section-content">
                                        <li>
                                            <h5 style="margin-bottom: 5px;font-weight: 800;">Day 4</h5>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">SEO</a>
                                            <div class="fr">
                                                <span class="duration">6 hour</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="section-content">
                                        <li>
                                            <h5 style="margin-bottom: 5px;font-weight: 800;">Day 5</h5>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">SEO</a>
                                            <div class="fr">
                                                <span class="duration">6 hour</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="section-content">
                                        <li>
                                            <h5 style="margin-bottom: 5px;font-weight: 800;">Day 6</h5>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">SMM- Facebook</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Instagram</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="section-content">
                                        <li>
                                            <h5 style="margin-bottom: 5px;font-weight: 800;">Day 7</h5>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">You Tube</a>
                                            <div class="fr">
                                                <span class="duration">2 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Google Anaytics</a>
                                            <div class="fr">
                                                <span class="duration">2 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Doubt Session</a>
                                            <div class="fr">
                                                <span class="duration">1 hour</span>
                                            </div>
                                        </li>
                                        <li style="margin: 10px 0px;border: 1px solid #e2e2e2;padding: 15px 20px;text-align: center;background: whitesmoke;">
                                            <div>
                                                <h5 style="margin-bottom: 10px;">Course Fee: <span style="font-weight: 700;">INR 18,000/-</span>( inclusive of service tax)</h5>
                                                <!--  <h5 style="">Minimum Participants: 20</h5> -->
                                            </div>
                                            <button class="flat-button bg-orange" data-toggle="modal" data-target="#enroll-modal" style="background: #65a2eb;border-color: #68a5ed;    margin-top: 15px;">Learn More</button>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="curriculum col-md-6" style="padding-left: 0;">
                                <li class="section">
                                    <!-- <h5 class="section-title" style="font-weight: 600;background: #eaeaea;padding: 25px 20px;">Advanced Digital Marketing program<br>70 hours of classroom training  ( 2 months)</h5> -->
                                    <h6 class="section-title" style="font-weight: 600;background: #eaeaea;padding: 25px 20px;">Advanced Digital Marketing program 80 hours<span style="font-weight: 100;    float: right;">2 months</span></h6>
                                    <ul class="section-content">
                                        <li class="course-lesson">
                                            <a class="lesson-title">An introduction to  Digital Marketing</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Website Creation</a>
                                            <div class="fr">
                                                <span class="duration">8 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Email Marketing</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Lead Generation</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Google Adwords</a>
                                            <div class="fr">
                                                <span class="duration">12 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">SEO</a>
                                            <div class="fr">
                                                <span class="duration">12 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Social Media Marketing</a>
                                            <div class="fr">
                                                <span class="duration">12 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Google Analytics</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Content Marketing</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Ecommerce Marketing</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Mobile Marketing</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Ad sense & Blogging</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Affliate marketing</a>
                                            <div class="fr">
                                                <span class="duration">3 hour</span>
                                            </div>
                                        </li>
                                        <li class="course-lesson">
                                            <a class="lesson-title">Doubt Session</a>
                                            <div class="fr">
                                                <span class="duration">2 hour</span>
                                            </div>
                                        </li>
                                        <li style="margin: 10px 0px;border: 1px solid #e2e2e2;padding: 15px 20px;text-align: center;background: whitesmoke;">
                                            <div>
                                                <h5 style="margin-bottom: 10px;">Course Fee: <span style="font-weight: 700;">INR 35,000/-</span>( inclusive of service tax)</h5>
                                                <!-- <h5 style="">This course will help the participants become a google certified professionals.</h5> -->
                                            </div>
                                            <button class="flat-button bg-orange" data-toggle="modal" data-target="#enroll-modal" style="background: #65a2eb;border-color: #68a5ed;    margin-top: 15px;">Learn More</button>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </section>
            <section class="flat-row about-us parallax parallax1">
                <div class="overlay bg-222">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="flat-counter">
                                    <div class="counter-content">
                                        <div class="numb-count" data-to="18" data-speed="2000" data-waypoint-active="yes">18</div>
                                        <div class="name-count">Year Of Experience</div>
                                        <!-- <div class="desc-count">Lorem ipsum dolor sit amet consecte tur adipiscing elit, sed do eiusmod tempor incididunt labore</div> -->
                                    </div>
                                </div>
                                <!-- /.flat-counter -->
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="flat-counter">
                                    <div class="counter-content">
                                        <div class="numb-counter">
                                            <div class="numb-count" data-to="59" data-speed="1000" data-waypoint-active="yes">0</div>
                                        </div>
                                        <div class="name-count">Courses Listed</div>
                                        <!-- <div class="desc-count">Lorem ipsum dolor sit amet consecte tur adipiscing elit, sed do eiusmod tempor incididunt labore</div> -->
                                    </div>
                                </div>
                                <!-- /.flat-counter -->
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="flat-counter">
                                    <div class="counter-content">
                                        <div class="numb-count" data-to="15" data-speed="2000" data-waypoint-active="yes">15</div>
                                        <div class="name-count">Professional Awards</div>
                                        <!-- <div class="desc-count">Lorem ipsum dolor sit amet consecte tur adipiscing elit, sed do eiusmod tempor incididunt labore</div> -->
                                    </div>
                                </div>
                                <!-- /.flat-counter -->
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="flat-counter">
                                    <div class="counter-content">
                                        <div class="numb-counter">
                                            <div class="numb-count" data-to="143" data-speed="1000" data-waypoint-active="yes">0</div>
                                        </div>
                                        <div class="name-count">Live Projects</div>
                                        <!-- <div class="desc-count">Lorem ipsum dolor sit amet consecte tur adipiscing elit, sed do eiusmod tempor incididunt labore</div> -->
                                    </div>
                                </div>
                                <!-- /.flat-counter -->
                            </div>
                        </div>
                        <!-- / .row -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.overlay -->
            </section>
            <section class="flat-row testimonial" style="    padding-bottom: 35px;">
                <div class="container">
                    <div class="flat-title-section">
                        <h1 class="title">TESTIMONIALS</h1>
                    </div>
                    <div class="testimonial-slider">
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <blockquote>
                                    The trainers are highly skilled and experienced. All being actively involved in the Live projects are enriched with solid experience.
                                </blockquote>
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-author">
                                    <strong class="author-name">Lima s Sebastian</strong>
                                    <div class="author-info">Btech ILAHIA College Of Engineering & Technology, Cochin</div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <blockquote>
                                    I can confidently say that Educ Kshetra project training center is the answer for all those aspirants who want to become Software developers / Certified Consultants. Also that the target or aim to go for this course is to land up with a better job and career prospects.
                                </blockquote>
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-author">
                                    <strong class="author-name">Sreeranjini Nandakumar</strong>
                                    <div class="author-info">Btech Sree Narayana Gurukulam College of Engineering, Cochin</div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <blockquote>
                                    I would definitely rejoin and recommend it to other friends and colleagues in the industry who are interested.
                                </blockquote>
                            </div>
                            <div class="testimonial-meta">
                                <div class="testimonial-author">
                                    <strong class="author-name">Vandana Gopinath</strong>
                                    <div class="author-info">Btech Indira Gandhi College Of Engineering, Cochin</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="flat-row pad-bottom-30" style="padding: 0;">
                <div class="flat-row join-us parallax parallax1 overlay bg-222" style="background-image: url(https://financialtribune.com/sites/default/files/field/image/17january/13_digital_-_580_-_da.jpg);">
                    <div class="overlay bg-222">
                        <div class="container">
                            <div class="row">
                                <div class="counter-content">
                                    <span class="counter-prefix">Join</span>
                                    <div class="numb-counter">
                                        <div class="numb-count" data-to="112093" data-speed="1000" data-waypoint-active="yes">0</div>
                                    </div>
                                    <span class="counter-suffix">people</span>
                                </div>
                                <p>We have our trainees undergo ahands on training approach thatconsists only 20% of theory while therest is taken up as real-time practicaltraining.</p>
                               
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container -->
                    </div>
                </div>
                <div class="container" style="    padding-top: 60px;padding-bottom: 35px;">
                    <div class="flat-title-section">
                        <h1 class="title">FAQ & CERTIFICATIONS</h1>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="flat-accordion">
                                <div class="flat-toggle">
                                    <h6 class="toggle-title">How the courses are designed?</h6>
                                    <div class="toggle-content">
                                        <div>
                                            <p>Courses are designed based on industry demands. Its end-to- end training and skills development solutions helps improve business productivity and enhances youth employability making Edu ckshetra the partner of choice for any skilling initiative.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /toggle -->
                                <div class="flat-toggle">
                                    <h6 class="toggle-title active">Why us?</h6>
                                    <div class="toggle-content">
                                        <div>
                                            <p>In today´s competitive world, preparedness is the key to success. Here you have to battle it out with the best of brains; and the one who wins will be the one who is better equipped. Fully armed and fiercely trained, with poorer skills and the perfect temperament of a true champion. That´s what Educ kshetra does to young aspirants like you- groom and guide you to your cherished goal.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /toggle -->
                                <div class="flat-toggle">
                                    <h6 class="toggle-title">Do you provide IT training?</h6>
                                    <div class="toggle-content">
                                        <p>Educ kshetra provides the widest array of integrated learning types to continuously develop and maintain IT skills – including short expert-led videos, video-based eLearning courses, live web-based instructor-led training, free live mentoring services, certification test preparation. We also help IT Professionals improve their soft skills to advance their careers.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.flat-accordion -->
                        </div>
                        <!-- / .col-md-6 -->
                        <div class="col-md-6">
                            <div class="flat-certificates">
                                <div class="flat-certified">
                                    <img src="images/cr1.jpg" />
                                    <!-- <h4>Google Certified</h4> -->
                                </div>
                                <div class="flat-certified">
                                    <img src="images/cr2.jpg" />
                                    <!-- <h4>Microsoft Certified</h4> -->
                                </div>
                                <div class="flat-certified">
                                    <img src="images/cr3.jpg" />
                                    <!-- <h4>Amazon Certified</h4> -->
                                </div>
                                <div class="flat-certified">
                                    <img src="images/cr4.jpg" />
                                    <!-- <h4>Google Certified</h4> -->
                                </div>
                                <div class="flat-certified">
                                    <img src="images/cr5.jpg" />
                                    <!-- <h4>Microsoft Certified</h4> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
            </section>
            <?php
            include ("footer.php");
            ?>
                </div>
                <!-- /. boxed -->
                <!-- Enroll Popup -->
                <div class="col-md-12">
                    <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#enroll-modal">Open Modal</button> -->
                    <!-- Modal -->
                    <div class="modal fade" id="enroll-modal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content col-md-12" style="padding: 15px;">
                                <!-- Form -->
                                <div class="col-md-12">
                                    <div id="respond" class="comment-respond contact style2">
                                        <h4 class="bold mgbt-17" style="text-transform: uppercase;">Enroll Now</h4>
                                        <div class="flat-contact-form style2 bg-dark height-small" >
                                            <div class="field clearfix">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" value="" tabindex="1" placeholder="Name" name="name" id="name_ft" required>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="email" value="" tabindex="1" placeholder="Email Id" name="email" id="email_ft" required>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="text" value="" tabindex="1" placeholder="Contact Number" name="contact" id="contact_ft" required>
                                                    </div>
                                                   
                                                </div>
                                               
                                                <div class="textarea-wrap">
                                                    <textarea class="type-input" tabindex="3" placeholder="Message" name="message" id="message-contact_ft" required></textarea>
                                                </div>
                                            </div>
                                            <div class="submit-wrap text-center ">
                                                <button class="flat-button bg-orange" id="btn_ft_submit" data-toggle="modal" data-target="#enroll-modal" style="background: #65a2eb;border-color: #68a5ed;">Submit Now</button>
                                            </div>
                                        </div>
                                        <!-- /.comment-form -->
                                    </div>
                                    <!-- /#respond -->
                                </div>
                                <!-- End -->
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal fade" id="Download-Curriculum-modal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content col-md-12" style="padding: 15px;">
                                <!-- Form -->
                                <div class="col-md-12">
                                    <div id="respond" class="comment-respond contact style2">
                                        <h4 class="bold mgbt-17" style="text-transform: uppercase;">Enroll Now</h4>
                                        <div class="flat-contact-form style2 bg-dark height-small">
                                       <!--  <form id="contactform" class="flat-contact-form style2 bg-dark height-small" method="post" action="#"> -->
                                            <div class="field clearfix">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" value="" tabindex="1" placeholder="Name" name="name" id="name_bhr" required>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="email" value="" tabindex="1" placeholder="Email Id" name="email" id="email_bhr" required>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="text" value="" tabindex="1" placeholder="Contact Number" name="contact" id="contact_bhr" required>
                                                    </div>
                                                   
                                                </div>
                                               
                                            </div>
                                            <div class="submit-wrap text-center">
                                                <button class="flat-button bg-orange" id="btn_bhr_submit"  style="background: #65a2eb;border-color: #68a5ed;">Submit Now</button>
                                            </div>
                                        </div>
                                       <!--  </form> -->
                                        <!-- /.comment-form -->
                                    </div>
                                    <!-- /#respond -->
                                </div>
                                <!-- End -->
                            </div>
                        </div>
                <!-- End -->
                <!-- Javascript -->
                <script type="text/javascript" src="javascript/jquery.min.js"></script>
                <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
                <script type="text/javascript" src="javascript/jquery.easing.js"></script>
                <script type="text/javascript" src="javascript/owl.carousel.js"></script>
                <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
                <script type="text/javascript" src="javascript/jquery-countTo.js"></script>
                <script type="text/javascript" src="javascript/parallax.js"></script>
                <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
                <script type="text/javascript" src="javascript/jquery-validate.js"></script>
                <script type="text/javascript" src="javascript/main.js"></script>
</body>

</html>