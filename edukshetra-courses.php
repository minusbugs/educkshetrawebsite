<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>educ kshetra - defining careers, transforming lives</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">
	
	<!-- Animation Style -->
    <!-- <link rel="stylesheet" type="text/css" href="stylesheets/animate.css"> -->

  

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head> 
<body class="header-sticky">
<div class="boxed">
     
        <?php
        include ('header.php');
        ?>

        <div class="page-title" style="background-image: url('assets/images/software courses.jpg');background-position: center !important;background-size: cover;background-attachment: fixed;"> 
        	<div class="overlay"></div>            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Our Courses</h2>
                        </div><!-- /.page-title-heading -->
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li>Courses</li>
                            </ul>                   
                        </div><!-- /.breadcrumbs --> 
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title parallax -->
    	
        <section class="main-content blog-posts flat-row course-grid">
            <div class="container">
                <div class="blog-title clearfix">
                    <h1 class="bold">ALL COURSES</h1>
                </div>
                <div class="row">
                    <div class="flat-post-ordering clearfix">
                        <div class="sort-views">
                            <label class="modern-select">
                                <select name="select_category" class="orderby" id="cat-select">
                                    <option value="0" selected="selected">Select Category</option>
                                    <option value="edukshetra-courses-networking.php">Networking</option>
                                    <option value="edukshetra-courses-software.php">Software</option>
                                    <option value="edukshetra-courses-embedded.php">Embedded</option>
                                </select>
                            </label>

                            <!-- <label class="modern-select">
                                 <select name="select_category" class="orderby">
                                    <option value="menu_order" selected="selected">Sort by</option>
                                    <option value="newest">Newest</option>
                                    <option value="oldest">Oldest</option>
                                </select>
                            </label> -->
                            <div class="list-grid">
                                <a data-layout = "course-grid" class="course-grid-view active" href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
                                <a data-layout = "course-list" class="course-list-view" href="#"><i class="fa fa-list" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="post-warp clearfix courses-edukshetra">

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Introduction to AngularJS. Understanding AngularJS Blocks. AngularJS - MVC Architecture.AngularJS Modules, Forms, etc… Ajax in AngularJS. AngularJS Services. Working with AngularJS. In Web Application Development. AngularJS Applications.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course1.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Angular JS</a> </h4>

                                <div class="price"> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p> Introduction to AngularJS. Understanding AngularJS Blocks. AngularJS - MVC Architecture.AngularJS Modules, Forms [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                JAVA Concepts, Introduction to Android, Application Structure (in detail), Emulator-Android Virtual Device, Basic UI design, Intents (in detail) Tabs and Tab Activity, SQLite Programming Adapters and Widgets, Advanced (JSON), Android Activities and UI Design, Advanced UI Programming, Application Structure, Toast, Menu, Dialog, List and Adapters, Multimedia Programming using Android, Database – SQLite, Location Based Services and Google Maps, Notifications, How to develop your own custom made Web browser, Android Development using other Tools, Testing and Debugging Android Application Installation of .apk,Advanced (JSON), Adapters and Widgets.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course2.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Android App Development</a> </h4>

                                <div class="price"> Mobile</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p> JAVA Concepts, Introduction to Android, Application Structure (in detail), Emulator-Android Virtual Device [. . .]</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Raw HTML5 +CSS3, HTML5 Canvas, Adobe Edge, Mobile Web and HTML5, PHP & MySQL Course Curriculum Introduction, Basic PHP Development, Control Structures Functions, Arrays Working With The File System, Working With Forms, Working With Regular Expressions, Classes And Objects, Introduction To Database, Cookies, Session, Disk Access, I/O, Math And Mail, Joomla ( Content Management System and Web Application Framework), Smarty (Library For Creating HTML Templates) PEAR (PHP Extension and Application Repository), Cake PHP (Rapid Development Framework), AJAX (Asynchronous JavaScript and XML)os Commerce (Open Source Shopping Cart) 
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course3.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Advanced HTML5</a> </h4>

                                <div class="price"> Web</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p> Raw HTML5 +CSS3, HTML5 Canvas, Adobe Edge, Mobile Web and HTML5, PHP & MySQL Course Curriculum Introduction [. . .]</p>
                            </div><!-- /.course-content -->
                        </div>


                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Introduction to Ethical Hacking - Footprinting and Reconnaissance – Scanning Networks – Enumeration – System Hacking – Malware Threats – Evading IDS, Firewalls and Honeypots – Sniffing – Social Engineering – Denial of Service – Session Hijacking – Hacking Web Servers – Hacking Web Applications – SQL Injection – Hacking Wireless Networks – Hacking Mobile Platforms – Cloud Computing – Cryptography
                            </p>

                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course4.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Certified Ethical Hacking </a> </h4>

                                <div class="price"> Networking</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p> Introduction to Ethical Hacking - Footprinting and Reconnaissance –Scanning Networks – Enumeration – System Hacking [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                • Big Data Introduction
                                • Hadoop Introduction
                                • What is Hadoop? Why Hadoop?
                                • Hadoop History?
                                • Different types of Components in Hadoop?
                                • HDFS, MapReduce, PIG, Hive, SQOOP, HBASE, OOZIE, Flume, Zookeeper and so on…
                                • What is the scope of Hadoop?
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course5.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Hadoop </a> </h4>

                                <div class="price"> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p>  Big Data Introduction,  Hadoop Introduction,  What is Hadoop? Why Hadoop?,  Hadoop History?,  Different types [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">The Raspberry Pi is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse. It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python. Our Raspberry Pi workshop has systematically designed so that the students can understand the basics well and can start from the scratch and programming its various features for any applications. Our workshop is purely hands-on and the students can write the code by their-own, test it using hardware.</p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course6.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Raspberry Pi </a> </h4>

                                <div class="price"><span style="font-family: initial;"></span> Embedded</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>25 Reviews</li>
                                </ul> 

                                <p>  The Raspberry Pi is a low cost, credit-card sized computerthat plugs into a computer monitor or TV, and uses [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>



                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Fundamentals- OOPS, Software Engineering, SQL Queries, Basics of Designing. Objective CData Type, NSInterger, NSNumber, Operators, LOOP, Introduction to .H and .M Files, Method Overloading, Mutable & Immutable strings, Mutable & Immutable Arrays, File Management. Introdcution to IPhone Architecture- Essential COCOA Touch Classes, Interface Builder, Nib File, COCOA & MVC Framework. Application Development in IPhone - Controls &Gestures, Controllers & Memory Management, Using Application Delegate, Managing Application Memory, Advanced Controllers Programming, Views, Navigation Based Application Development. Database – SqLite, Creating outlets & Actions, Parsing Data with SqLite - Projects.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/ios.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>iOS </a> </h4>
                                <div class="price"> Mobile</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>Rating</li>
                                </ul> 
                                <p>  Fundamentals- OOPS, Software Engineering, SQL Queries, Basics of Designing. Objective CData Type, NSInterger [. . .].</p>
                            </div>
                        </div>


                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Introduction to Bootstrap. Responsive web page design using Grid System. Implementing Bootstrap with CSS Classes. Implementing Bootstrap Components. Implementing Bootstrap JavaScript.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/bootstrap.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Bootstrap</a> </h4>

                                <div class="price"> Web</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p>  Introduction to Bootstrap. Responsive web page design using Grid System.Implementing Bootstrap with CSS Classes. [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>


                         <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">Arduino is a tool for making computers that can sense and control more of the physical world than your desktop computer. It's an open-source physical computing platform based on a simple microcontroller board, and a development environment for writing software for the board. Our workshop is very systematically designed so that the students can understand the basics well, can do programming of its various peripherals and special features. Our workshop is purely hands-on and the students can write the code by their-own, test it using Simulator and final testing using PIC trainer kits.</p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/arduino.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>Arduino Programming </a> </h4>
                                <div class="price"><span style="font-family: initial;"></span> Embedded</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>25 Reviews</li>
                                </ul> 
                                <p>  Arduino is a tool for making computers that can sense and control more of the physical world than your  [. . .].</p>
                            </div>
                        </div>


                        
                    </div><!-- / .post-wrap -->

                    <div class="blog-pagination">

                        <div class="col-md-12 text-center  loader-animation" style="display:none;">
                            <img src="http://assets.motherjones.com/interactives/projects/features/koch-network/shell19/img/loading.gif" class="img-responsive" style="margin: 0 auto;">
                        </div>

                        <div class="button-wrap">
                            <button type="button" id="subscribe-button" class="subscribe-button load-more-course" title="Subscribe now" get-course="course_sec2.html"> Load more courses </button>
                        </div>
                    </div>
                </div><!-- /row -->
            </div><!-- /container -->
        </section><!-- /main-content -->


            <?php
            include ("footer.php");
            ?>
        </div> <!-- /.boxed -->

        <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script> 
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
   
    <script type="text/javascript" src="javascript/jquery-validate.js"></script>     
    <script type="text/javascript" src="javascript/main.js"></script>


    <script>
        //courses-edukshetra
        $('.load-more-course').click(function(){
            
            //loader-animation
            //load-more-course
            $('.loader-animation').show();
            $('.load-more-course').hide();            

            getAddress = $(this).attr('get-course');
            console.log("get address: ",getAddress); 

            if(getAddress == 'course_sec2.html'){
                $.ajax({
                url: getAddress,
                type: 'post',
                success: function (response) {
                    /*Re call function*/
                    setTimeout(function(){  
                     $('.flat-course').click(function(){
                        getImage = $(this).find('img').attr('src');
                        getContent = $(this).find('.course-detail-content').html();
                        getContentTitle = $(this).find('h4 a').html();
                        getContentTag = $(this).find('.price').html();


                        $('#course-detail-modal img').attr('src',getImage);
                        $('#course-detail-modal .c-content').html(getContent);
                        $('#course-detail-modal h4 a').html(getContentTitle);
                        $('#course-detail-modal .price').html(getContentTag);


                        $('#course-detail-modal').modal('toggle');
                     });
                     }, 1000);
                     /*End*/
                   //console.log(response);
                   $('.courses-edukshetra').append(response);
                   $('.loader-animation').hide();
                   $('.load-more-course').show();
                   //$('.load-more-course').remove();
                   $('.load-more-course').attr("get-course","course_sec3.html");
                }
                });
            }
            if(getAddress == 'course_sec3.html'){
                $.ajax({
                url: getAddress,
                type: 'post',
                success: function (response) {
                    /*Re call function*/
                    setTimeout(function(){  
                     $('.flat-course').click(function(){
                        getImage = $(this).find('img').attr('src');
                        getContent = $(this).find('.course-detail-content').html();
                        getContentTitle = $(this).find('h4 a').html();
                        getContentTag = $(this).find('.price').html();


                        $('#course-detail-modal img').attr('src',getImage);
                        $('#course-detail-modal .c-content').html(getContent);
                        $('#course-detail-modal h4 a').html(getContentTitle);
                        $('#course-detail-modal .price').html(getContentTag);


                        $('#course-detail-modal').modal('toggle');
                     });
                     }, 1000);
                     /*End*/
                   //console.log(response);
                   $('.courses-edukshetra').append(response);
                   $('.loader-animation').hide();
                   $('.load-more-course').show();
                   $('.load-more-course').attr("get-course","course_sec4.html");
                }
                });
            } 
            if(getAddress == 'course_sec4.html'){
                $.ajax({
                url: getAddress,
                type: 'post',
                success: function (response) {
                    /*Re call function*/
                    setTimeout(function(){  
                     $('.flat-course').click(function(){
                        getImage = $(this).find('img').attr('src');
                        getContent = $(this).find('.course-detail-content').html();
                        getContentTitle = $(this).find('h4 a').html();
                        getContentTag = $(this).find('.price').html();


                        $('#course-detail-modal img').attr('src',getImage);
                        $('#course-detail-modal .c-content').html(getContent);
                        $('#course-detail-modal h4 a').html(getContentTitle);
                        $('#course-detail-modal .price').html(getContentTag);


                        $('#course-detail-modal').modal('toggle');
                     });
                     }, 1000);
                     /*End*/
                   //console.log(response);
                   $('.courses-edukshetra').append(response);
                   $('.loader-animation').hide();
                   $('.load-more-course').remove();
                   // $('.load-more-course').show();
                   // $('.load-more-course').attr("get-course","course_sec3.html");
                }
                });
            }         
        });
    </script>
    <script>
     $('.flat-course').click(function(){
        getImage = $(this).find('img').attr('src');
        getContent = $(this).find('.course-detail-content').html();
        getContentTitle = $(this).find('h4 a').html();
        getContentTag = $(this).find('.price').html();


        $('#course-detail-modal img').attr('src',getImage);
        $('#course-detail-modal .c-content').html(getContent);
        $('#course-detail-modal h4 a').html(getContentTitle);
        $('#course-detail-modal .price').html(getContentTag);
        $('#course-detail-modal').modal('toggle');
     });
    </script>
    <script>
        $('#cat-select').change(function(){
            if($(this).val() != 0){
                window.location.replace($(this).val());
            }
        });
    </script>


</body>
</html>