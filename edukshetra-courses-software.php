<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>educ kshetra - defining careers, transforming lives</title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">
	
	<!-- Animation Style -->
    <!-- <link rel="stylesheet" type="text/css" href="stylesheets/animate.css"> -->

  

    <!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head> 
<body class="header-sticky">
<div class="boxed">
     
        <?php
        include ('header.php');
        ?>

        <div class="page-title parallax parallax4" style="background-image: url('assets/images/software courses.jpg');background-position: center !important;background-size: cover;background-attachment: fixed;"> 
        	<div class="overlay"></div>            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Software Courses</h2>
                        </div><!-- /.page-title-heading -->
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="index.html">Home</a></li>
                                <li>Courses</li>
                            </ul>                   
                        </div><!-- /.breadcrumbs --> 
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title parallax -->
    	
        <section class="main-content blog-posts flat-row course-grid">
            <div class="container">
                <div class="blog-title clearfix">
                    <h1 class="bold">COURSES</h1>
                </div>
                <div class="row">
                    <div class="flat-post-ordering clearfix">
                        <div class="sort-views">
                            <label class="modern-select">
                                <select name="select_category" class="orderby" id="cat-select">
                                    <option value="0">Select Category</option>
                                    <option value="edukshetra-courses-networking.php">Networking</option>
                                    <option value="edukshetra-courses-software.php" selected="selected">Software</option>
                                    <option value="edukshetra-courses-embedded.php">Embedded</option>
                                </select>
                            </label>

                            <!-- <label class="modern-select">
                                 <select name="select_category" class="orderby">
                                    <option value="menu_order" selected="selected">Sort by</option>
                                    <option value="newest">Newest</option>
                                    <option value="oldest">Oldest</option>
                                </select>
                            </label> -->
                            <div class="list-grid">
                                <a data-layout = "course-grid" class="course-grid-view active" href="#"><i class="fa fa-th" aria-hidden="true"></i></a>
                                <a data-layout = "course-list" class="course-list-view" href="#"><i class="fa fa-list" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="post-warp clearfix courses-edukshetra">

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Introduction to AngularJS. Understanding AngularJS Blocks. AngularJS - MVC Architecture.AngularJS Modules, Forms, etc… Ajax in AngularJS. AngularJS Services. Working with AngularJS. In Web Application Development. AngularJS Applications.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course1.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Angular JS</a> </h4>

                                <div class="price"> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p> Introduction to AngularJS. Understanding AngularJS Blocks. AngularJS - MVC Architecture.AngularJS Modules, Forms [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                • Big Data Introduction
                                • Hadoop Introduction
                                • What is Hadoop? Why Hadoop?
                                • Hadoop History?
                                • Different types of Components in Hadoop?
                                • HDFS, MapReduce, PIG, Hive, SQOOP, HBASE, OOZIE, Flume, Zookeeper and so on…
                                • What is the scope of Hadoop?
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course5.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Hadoop </a> </h4>

                                <div class="price"> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p>  Big Data Introduction,  Hadoop Introduction,  What is Hadoop? Why Hadoop?,  Hadoop History?,  Different types [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                JAVA Concepts, Introduction to Android, Application Structure (in detail), Emulator-Android Virtual Device, Basic UI design, Intents (in detail) Tabs and Tab Activity, SQLite Programming Adapters and Widgets, Advanced (JSON), Android Activities and UI Design, Advanced UI Programming, Application Structure, Toast, Menu, Dialog, List and Adapters, Multimedia Programming using Android, Database – SQLite, Location Based Services and Google Maps, Notifications, How to develop your own custom made Web browser, Android Development using other Tools, Testing and Debugging Android Application Installation of .apk,Advanced (JSON), Adapters and Widgets.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course2.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Android App Development</a> </h4>

                                <div class="price"> Mobile</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p> JAVA Concepts, Introduction to Android, Application Structure (in detail), Emulator-Android Virtual Device [. . .]</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Raw HTML5 +CSS3, HTML5 Canvas, Adobe Edge, Mobile Web and HTML5, PHP & MySQL Course Curriculum Introduction, Basic PHP Development, Control Structures Functions, Arrays Working With The File System, Working With Forms, Working With Regular Expressions, Classes And Objects, Introduction To Database, Cookies, Session, Disk Access, I/O, Math And Mail, Joomla ( Content Management System and Web Application Framework), Smarty (Library For Creating HTML Templates) PEAR (PHP Extension and Application Repository), Cake PHP (Rapid Development Framework), AJAX (Asynchronous JavaScript and XML)os Commerce (Open Source Shopping Cart) 
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/course3.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Advanced HTML5</a> </h4>

                                <div class="price"> Web</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p> Raw HTML5 +CSS3, HTML5 Canvas, Adobe Edge, Mobile Web and HTML5, PHP & MySQL Course Curriculum Introduction [. . .]</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Fundamentals- OOPS, Software Engineering, SQL Queries, Basics of Designing. Objective CData Type, NSInterger, NSNumber, Operators, LOOP, Introduction to .H and .M Files, Method Overloading, Mutable & Immutable strings, Mutable & Immutable Arrays, File Management. Introdcution to IPhone Architecture- Essential COCOA Touch Classes, Interface Builder, Nib File, COCOA & MVC Framework. Application Development in IPhone - Controls &Gestures, Controllers & Memory Management, Using Application Delegate, Managing Application Memory, Advanced Controllers Programming, Views, Navigation Based Application Development. Database – SqLite, Creating outlets & Actions, Parsing Data with SqLite - Projects.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/ios.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>iOS </a> </h4>
                                <div class="price"> Mobile</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>Rating</li>
                                </ul> 
                                <p>  Fundamentals- OOPS, Software Engineering, SQL Queries, Basics of Designing. Objective CData Type, NSInterger [. . .].</p>
                            </div>
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                .NET Frameworks, Web Programming Concepts, C# Advanced Features, LINQ Windows application programming, Database Connectivity Using ADO.NET, Three Tier Application Modelling, Web application programming, HTML and CSS, Creating and Styling HTML5 Pages, JavaScript, ASP.NET Web Application Development State Management, Customizing Web Applications Designing and Implementing Databases with MS SQL Server 2014, ADO.NET Data Controls, Crystal Report, ASP.NET Ajax, Web Services, JQuery, Validation, Bootstrap, Handling Emails, Deployment ASP.NET Web Hosting.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/net.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>ASP.Net</a> </h4>

                                <div class="price"> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p> .NET Frameworks, Web Programming Concepts, C# Advanced Features, LINQ Windows application programming, [. . .]</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                • Enabling Technologies and System Models for Cloud Computing
                                • Introduction to Cloud Computing including benefits, challenges, and risks
                                • Cloud Computing Models including Infrastructure/Platform/Software – as-a-service
                                • Public cloud, private cloud and hybrid clouds
                                • Cloud OS
                                • Cloud Architectures including Federated Clouds
                                • Scalability, Performance, QoS
                                • Data centers for Cloud Computing
                                • Principles of Virtualization platforms
                                • Security and Privacy issues in the Cloud
                                • VMWare ESX Memory Management
                                • Capacity Planning and Disaster Recovery in Cloud Computing
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/cloud.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Cloud Computing </a> </h4>

                                <div class="price"> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p>  Enabling Technologies and System Models for Cloud Computing, Introduction to Cloud Computing including benefits [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Introduction to JQuery. Implement JQuery in Web Application. Working with Jquery Events. Working with JQuery Animation. Implementing Jquery UI. jQuery Validation. jQuery Data transition. jQuery data transition using Ajax and JSON.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/jquery.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>Jquery </a> </h4>

                                <div class="price"> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p>  Introduction to JQuery. Implement JQuery in Web Application. Working with Jquery Events. Working with JQuery Animation.  [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Beyond the Hype - Big Data Skills and Sources of Big Data, Big Data Adoption, What is Big Data - Characteristics of Big Data - The Four V's, Understanding Big Data with Examples, The Big Data Platform - Key aspects of a Big Data Platform, Governance for Big Data, Five High Value Big Data Use Cases - Overview of High Value Big Data Use Cases, Examples, Technical Details of Big Data Components - Text Analytics and Streams, Cloud and Big Data.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/bigdata.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>Big Data  </a> </h4>
                                <div class="price"> Software</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>Rating</li>
                                </ul> 
                                <p>  Beyond the Hype - Big Data Skills and Sources of Big Data, Big Data Adoption, What is Big Data - Characteristics [. . .].</p>
                            </div>
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                               SQL, SQL Plus, Database, Objects, SQL DBA, SQL Plus, Database Objects, Objects Type, Developer 6.0, Featuring Forms, Reports
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/oracle.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>Oracle 10g with Developer</a> </h4>
                                <div class="price"> Software</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>Rating</li>
                                </ul> 
                                <p>  SQL, SQL Plus, Database, Objects, SQL DBA, SQL Plus, Database Objects, Objects Type, Developer 6.0  [. . .].</p>
                            </div>
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                General Introduction, Introduction to SharePoint, SharePoint Installation and Administration, SharePoint Site Hierarchy, Lists, Create View, Predefined List & Libraries, Communication Lists,Tracking ListsUsers, Groups and Permissions,Claim Based Authentication, Enterprise Content Management, User Profile Service Administration, Administering and configuring Search, Business Intelligence, SharePoint API, SharePoint Tools for Visual Studio 2010, Feature Types, Lists and Event Handling, Working with Web Parts, Sandboxed Solutions,InfoPath, Business Connectivity Service, Workflow, Working with external data sources, Working with Workflows, SharePoint Designer.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/sharepoint.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>SharePoint </a> </h4>

                                <div class="price"> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p>  General Introduction, Introduction to SharePoint, SharePoint Installation and Administration, SharePoint [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                RMI, XML, Java Mail API, Java Message Service [JMS], Web Services, Enterprise Java Beans, Hibernate, Spring, Struts, AJAX.
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>

                                <a><img src="images/index/j2ee.jpg" alt="Course1"></a>
                            </div><!-- /.featured-post -->

                            <div class="course-content">
                                <h4><a>J2EE </a> </h4>

                                <div class="price"> Software</div>    
                                
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>

                                    <li>Rating</li>
                                </ul> 

                                <p>  RMI, XML, Java Mail API, Java Message Service [JMS], Web Services, Enterprise Java Beans,Hibernate, Spring [. . .].</p>
                            </div><!-- /.course-content -->
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Introduction, Basic PHP Development, Control Structures, Functions Working With The File System, Working With Forms, Working With Regular Expressions, Classes And Objects, Introduction To Database, Cookies, Session Disk Access, I/O, Math And Mail, Joomla (Content Management System And Web Application Framework), Smarty (Library For Creating HTML Templates) PEAR (PHP Extension and Application Repository), Cake PHP (Rapid Development Framework), AJAX (Asynchronous JavaScript and XML), osCommerce (Open Source Shopping Cart)
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/php.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>PHP & MySQL </a> </h4>
                                <div class="price"> Software</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>Rating</li>
                                </ul> 
                                <p>  Introduction, Basic PHP Development, Control Structures, Functions Working With The File System, Working [. . .].</p>
                            </div>
                        </div>

                        <div class="flat-course flat-hover-zoom">
                            <p class="course-detail-content hidden">
                                Advanced Java Programming (J2EE) (Web-Server & Support Technologies). Java Database Connectivity, Servlets, Servlet Interaction & Advanced Servlets, JavaServerPages, RMI, EJB, XML, Structs, MVC Architecture, Hibernate, Overview of the Course XML, Servlet, JSP, JDBC, RMI, Introduction to XML Markup, Document Type Definition, XML Schema, Handling HTTP get Requests, Multi-Tier Applications, JBCD from Servelet, JavaBeans Component Model
                            </p>
                            <div class="featured-post">             
                                <div class="overlay">
                                    <div class="link"></div>
                                </div>
                                <a><img src="images/index/advjava.jpg" alt="Course1"></a>
                            </div>
                            <div class="course-content">
                                <h4><a>Advanced Java </a> </h4>
                                <div class="price"> Software</div>
                                <ul class="course-meta review">
                                    <li class="review-stars">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half"></i>
                                        <i class="fa fa-star-o"></i>
                                    </li>
                                    <li>Rating</li>
                                </ul> 
                                <p>  Java Database Connectivity, Servlets, Servlet Interaction & Advanced Servlets, JavaServerPages [. . .].</p>
                            </div>
                        </div>


                        
                    </div><!-- / .post-wrap -->
                </div><!-- /row -->
            </div><!-- /container -->
        </section><!-- /main-content -->    




            <?php
            include ("footer.php");
            ?>
        </div> <!-- /.boxed -->

        <!-- Javascript -->
    <script type="text/javascript" src="javascript/jquery.min.js"></script>
    <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/jquery.easing.js"></script> 
    <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
    <script type="text/javascript" src="javascript/parallax.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
   
    <script type="text/javascript" src="javascript/jquery-validate.js"></script>     
    <script type="text/javascript" src="javascript/main.js"></script>


    <script>
     $('.flat-course').click(function(){
        getImage = $(this).find('img').attr('src');
        getContent = $(this).find('.course-detail-content').html();
        getContentTitle = $(this).find('h4 a').html();
        getContentTag = $(this).find('.price').html();


        $('#course-detail-modal img').attr('src',getImage);
        $('#course-detail-modal .c-content').html(getContent);
        $('#course-detail-modal h4 a').html(getContentTitle);
        $('#course-detail-modal .price').html(getContentTag);


        $('#course-detail-modal').modal('toggle');
     });
    </script>
    <script>
        $('#cat-select').change(function(){
            if($(this).val() != 0){
                window.location.replace($(this).val());
            }
        });
    </script>

</body>
</html>